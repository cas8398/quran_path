{
    "id": 3039,
    "surah_id": 26,
    "ayah": 107,
    "page": 371,
    "quarter_hizb": 37.75,
    "juz": 19,
    "manzil": 5,
    "arabic": "\u0627\u0650\u0646\u0651\u0650\u064a\u0652 \u0644\u064e\u0643\u064f\u0645\u0652 \u0631\u064e\u0633\u064f\u0648\u0652\u0644\u064c \u0627\u064e\u0645\u0650\u064a\u0652\u0646\u064c \u06d9  ",
    "latin": "Inn\u012b lakum ras\u016blun am\u012bn(un).",
    "translation": "Sesungguhnya aku adalah seorang rasul tepercaya (yang diutus) kepadamu.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 26,
        "arabic": "\u0627\u0644\u0634\u0651\u0639\u0631\u0627\u06e4\u0621",
        "latin": "Asy-Syu\u2018ar\u0101'",
        "transliteration": "Asy-Syu\u2018ara'",
        "translation": "Para Penyair",
        "num_ayah": 227,
        "page": 367,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Nabi Nuh melanjutkan dakwahnya, \u201cSesungguhnya aku ini seorang rasul kepercayaan yang diutus kepadamu, untuk menyampaikan pesan-pesan Tuhanku kepadamu.",
        "tahlili": "Nabi Nuh memberitahu kaumnya bahwa ia adalah seorang rasul Allah yang diutus kepada mereka. Dia dipercaya untuk menyampaikan perintah dan larangan Allah, tanpa menambah atau mengurangi sedikit pun.\nPada Surah H\u016bd/11: 31 diterangkan bahwa Nabi Nuh tidak mempunyai kekayaan yang akan diberikan kepada kaumnya. Oleh karena itu, ia tidak dapat menjanjikan harta dan kekayaan untuk mereka. Ia juga tidak mengetahui hal-hal yang gaib, tidak pernah mengatakan bahwa ia adalah malaikat, dan tidak menjanjikan kesenangan dan kebahagiaan kepada orang-orang yang mengikuti seruannya. Semuanya itu hanya Allah yang mengetahui, memiliki, dan menentukan, karena Dialah Yang Mahakuasa. Nuh hanya bertugas untuk menyampaikannya.",
        "intro_surah": "Surah ini terdiri dari 227 ayat, termasuk kelompok surah-surah Makkiyyah. Dinamakan asy-Syu\u2019ar\u0101\u2019 (kata jamak dari asy-sy\u0101\u2019ir yang berarti penyair) diambil dari kata \u201casy-Syu\u2019ar\u0101\u2019\u201d yang terdapat pada ayat 224, yaitu pada bagian terakhir surah ini, ketika Allah swt secara khusus menyebutkan kedudukan penyair-penyair. Para penyair itu mempunyai sifat-sifat yang jauh berbeda dengan rasul-rasul. Mereka diikuti oleh orang-orang yang sesat dan suka memutarbalikkan lidah serta tidak mempunyai pendirian, per-buatannya tidak sesuai dengan apa yang diucapkan. Sifat-sifat yang demikian itu tidaklah sekali-kali dimiliki para rasul. Oleh karena itu, tidak patut bila Nabi Muhammad dituduh sebagai penyair dan Al-Qur\u2019an dianggap sebagai syair. Al-Qur\u2019an adalah wahyu Allah bukan buatan manusia.\n\nPokok-pokok Isinya:\n1. \tKeimanan:\nJaminan Allah akan kemenangan perjuangan dan keselamatan para rasul-Nya. Al-Qur\u2019an benar-benar wahyu Allah yang diturunkan ke dunia melalui Malaikat Jibril a.s. (R\u016b\u1e25ul-Am\u012bn); hanya Allah yang wajib disembah.\n2. \tHukum-hukum:\nKeharusan menyempurnakan takaran dan timbangan, larangan mengubah syair yang berisi caci maki, khurafat, dan kebohongan.\n3. \tKisah-kisah:\nKisah Nabi Musa dengan Fir\u2018aun, Kisah Nabi Ibrahim, Nabi Nuh, Nabi Hud, Nabi Saleh, dan Nabi Lut dengan kaum mereka masing-masing; serta kisah Nabi Syuaib dengan penduduk Aikah.\n4. \tLain-lain:\nKebinasaan suatu bangsa/umat karena meninggalkan petunjuk-petunjuk agama, tumbuh-tumbuhan yang beraneka ragam dan perubahan-perubahan yang terjadi atasnya adalah bukti kekuasaan Tuhan Yang Maha Esa. Petunjuk-petunjuk Allah bagi para pemimpin agar berlaku lemah-lembut terhadap pengikut mereka. Al-Qur\u2019an turun dalam bahasa Arab sebagaimana disebutkan dalam kitab-kitab suci terdahulu.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-FURQ\u0100N DENGAN\nSURAH ASY-SYU\u2019AR\u0100\u2019\n\n1. \tBeberapa persoalan dalam Surah al-Furq\u0101n diuraikan kembali secara luas dalam Surah asy-Syu\u2019ar\u0101\u2019 antara lain kisah para nabi.\n2. \tMasing-masing dari kedua surah itu dimulai dengan keterangan dari Allah bahwa Al-Qur\u2019an adalah petunjuk bagi manusia dan pembeda antara yang hak dengan yang batil dan ditutup dengan ancaman bagi orang yang mendustakan Allah.\n",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menerangkan kepada Rasulullah kisah Nabi Ibrahim, nenek moyang bangsa Arab, yang diperintahkan untuk menyeru kaumnya agar beriman. Diterangkan juga penolakan kaumnya untuk beriman kepada Allah dan meninggalkan kesyirikan. Pada ayat-ayat ini, Allah menerangkan kisah Nabi Nuh, nabi yang ketiga di antara nabi-nabi yang diutus Allah setelah Nabi Adam dan Nabi Idris. Apa yang terjadi pada Nabi Ibrahim, juga sudah terjadi sebelumnya pada Nabi Nuh. Ia didustakan oleh kaumnya termasuk anaknya sendiri. Akibat keingkaran itu, Allah menimpakan kepada kaum Nabi Nuh azab berupa angin topan dan banjir yang memusnahkan mereka. Adapun Nuh beserta orang-orang yang mengikuti seruannya diselamatkan Allah.",
        "theme_group": "KISAH NABI NUH DAN KAUMNYA",
        "kosakata": "Kosakata: Ar\u017cal\u016bn \u0627\u064e\u0631\u0652\u0630\u064e\u0644\u064f\u0648\u0652\u0646\u064e  (asy-Syu\u2019ar\u0101\u2019/26: 111)\nAl-Ar\u017cal\u016bn artinya \u201corang-orang yang rendah derajat dan martabatnya, bukan kelompok bermartabat tinggi. Kelompok rendahan ini biasanya merupakan pengikut para nabi yang diutus Allah kepada manusia. Sedang-kan kelompok orang-orang penting, walaupun ada yang beriman dan menjadi pengikut nabi, tetapi jumlah mereka tidak banyak dibanding dengan kelompok rendahan tersebut. Sebutan orang-orang rendahan untuk para pengikut nabi yang disinggung dalam ayat ini, diberikan oleh mereka yang menentang dan mendustakan nabi dengan maksud menghinakan. Dengan beriman kepada Allah dan nabi serta mengikuti seruannya, justru merekalah yang meraih keberuntungan dan derajat tinggi.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}