{
    "id": 5978,
    "surah_id": 88,
    "ayah": 11,
    "page": 592,
    "quarter_hizb": 60,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0644\u0651\u064e\u0627 \u062a\u064e\u0633\u0652\u0645\u064e\u0639\u064f \u0641\u0650\u064a\u0652\u0647\u064e\u0627 \u0644\u064e\u0627\u063a\u0650\u064a\u064e\u0629\u064b \u06d7",
    "latin": "L\u0101 tasama\u2018u f\u012bh\u0101 l\u0101giyah(tan).",
    "translation": "Di sana kamu tidak mendengar (perkataan) yang tidak berguna.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 88,
        "arabic": " \u0627\u0644\u063a\u0627\u0634\u064a\u0629",
        "latin": "Al-G\u0101syiyah",
        "transliteration": "Al-Gasyiyah",
        "translation": "Hari Kiamat Yang Menghilangkan Kesadaran",
        "num_ayah": 26,
        "page": 592,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Di surga sana kamu tidak mendengar perkataan yang tidak berguna. Tidak ada perkataan kotor, umpatan, ungkapan kemarahan, dan semisalnya. Di sana mereka hanya mendengar hal-hal yang menyenangkan.",
        "tahlili": "Dalam ayat-ayat berikut ini, Allah menerangkan keadaan surga:\na.\tSurga tempatnya bernilai tinggi, lebih tinggi dari nilai tempat-tempat yang lain.\nb.\tDi dalamnya tidak terdengar perkataan yang tidak berguna, sebab tempat itu adalah tempat orang-orang yang dikasihi Allah.\nc.\tDi dalamnya terdapat mata air yang mengalirkan air bersih yang menarik pandangan bagi siapa saja yang melihatnya.\nd.\tDi dalamnya terdapat mahligai yang tinggi.\ne.\tDi dekat mereka tersedia gelas-gelas yang berisi minuman yang sudah siap diminum.\nf.\tDi dalamnya terdapat bantal-bantal tersusun yang dapat dipergunakan menurut selera mereka, duduk di atasnya atau dipakai untuk bersandar dan sebagainya.\ng.\tDi sana terdapat pula permadani yang indah dan terhampar pada setiap tempat.\nh.\tTerdapat segala macam kenikmatan rohani dan jasmani yang jauh dari yang dapat kita bayangkan.",
        "intro_surah": "Surah ini terdiri dari 26 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah a\u017c-\u017b\u0101riy\u0101t. Nama al-G\u0101syiyah diambil dari kata al-g\u0101syiyah yang terdapat pada ayat pertama surah ini yang artinya peristiwa yang dahsyat; tetapi yang dimaksud adalah hari Kiamat. Surah ini adalah surah yang kerap kali dibaca Nabi pada rakaat kedua pada salat Hari Raya dan salat Jum\u2019at.\n\nPokok-pokok Isinya:\nKeterangan tentang orang-orang kafir pada hari Kiamat dan azab yang dijatuhkan atas mereka. Keterangan tentang orang-orang yang beriman serta keadaan surga yang diberikan kepada mereka sebagai balasan. Perintah untuk memperhatikan keajaiban ciptaan-ciptaan Allah. Perintah kepada Rasulullah saw untuk memperingatkan kaumnya kepada ayat-ayat Allah karena beliau adalah seorang pemberi peringatan, dan bukanlah seorang yang berkuasa atas keimanan mereka.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-A\u2018L\u0100 DENGAN\nSURAH AL-G\u0100SYIYAH\n\nPada Surah al-A\u2018l\u0101 diterangkan secara umum tentang orang yang beriman, orang yang kafir, surga dan neraka. Kemudian dalam Surah al-G\u0101syiyah dikemukakan kembali dengan cara yang lebih luas.",
        "munasabah_prev_theme": "Pada ayat-ayat terdahulu dijelaskan bahwa orang-orang kafir akan dimasukkan ke dalam neraka pada hari Kiamat. Makanan mereka adalah pohon berduri dan minuman mereka diambil dari mata air yang sangat panas. Kedua makanan dan minuman itu tidak pernah mengenyangkan, bahkan selalu merasa haus dan lapar. Pada ayat-ayat berikut ini dijelaskan kehidupan orang-orang beriman. Wajah mereka berseri-seri, tempat mereka adalah surga yang di dalamnya terdapat minuman dan makanan yang lezat serta kedamaian dan kesenangan.",
        "theme_group": "KEADAAN PENGHUNI SURGA",
        "kosakata": "Kosakata:\n1.\tL\u0101giyah \u0644\u064e\u0627\u063a\u0650\u064a\u064e\u0629 (al-G\u0101syiyah/88: 11)\nBentuk isim f\u0101\u2018il dari fi\u2018il m\u0101\u1e0d\u012b \u201clag\u0101-yalg\u016b-lagwan\u201d. Akar katanya adalah (l\u0101m-gain-huruf \u2018illat) artinya sesuatu yang tidak berarti. Al-lagwu dikatakan untuk perkataan yang keluar dari seseorang tanpa dipikirkan terlebih dahulu. Perkataan yang buruk, jelek juga dinamakan \u201clagw\u201d. Ayat ini menjelaskan bahwa penghuni surga tidak akan mendengarkan sesuatu yang tidak ada artinya seperti bohong, cercaan, mengatakan yang tidak baik kepada orang lain dan lain sebagainya. Semua perkataan mereka mengandung hikmah dan berguna. Kata \u201cl\u0101giyah\u201d sendiri berbentuk isim nakirah dalam konteks menafikan sesuatu, maka mengandung arti umum yaitu tidak adanya perkataan apapun yang tidak berguna. \n\n2.\tZar\u0101biyy \u0632\u064e\u0631\u064e\u0627\u0628\u0650\u064a\u0651 (al-G\u0101syiyah/88: 16)\nBentuk jamak dari zirbiyyah dan zirbiyy artinya permadani. Al-Farra\u2032 mengartikannya dengan permadani yang mempunyai sabut-sabut yang lembut (\u1e6dan\u0101fis lah\u0101 khamlun raq\u012bq). Ayat ini menjelaskan bahwa permadani untuk penghuni surga terhampar dengan jumlah yang banyak. Bentuknya sangat indah dengan sabut-sabut yang halus. Dalam hakikatnya permadani di surga sangat jauh berbeda dengan permadani yang ada di dunia. Semuanya hanya penggambaran saja, disesuaikan dengan daya imajinasi manusia di dunia.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}