{
    "id": 5383,
    "surah_id": 70,
    "ayah": 8,
    "page": 568,
    "quarter_hizb": 57.5,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u064a\u064e\u0648\u0652\u0645\u064e \u062a\u064e\u0643\u064f\u0648\u0652\u0646\u064f \u0627\u0644\u0633\u0651\u064e\u0645\u064e\u0627\u06e4\u0621\u064f \u0643\u064e\u0627\u0644\u0652\u0645\u064f\u0647\u0652\u0644\u0650\u06d9",
    "latin": "Yauma tak\u016bnus-sam\u0101'u kal-muhl(i).",
    "translation": "(Siksaan itu datang) pada hari (ketika) langit menjadi seperti luluhan perak,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 70,
        "arabic": " \u0627\u0644\u0645\u0639\u0627\u0631\u062c",
        "latin": "Al-Ma\u2018\u0101rij",
        "transliteration": "Al-Ma\u2018arij",
        "translation": "Tempat-Tempat Naik",
        "num_ayah": 44,
        "page": 568,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Ingatlah siksa yang akan dialami oleh kaum kafir itu akan terjadi pada hari ketika langit yang sehari-harinya terlihat kokoh menjadi bagaikan cairan tembaga,",
        "tahlili": "Dalam ayat ini, Allah menerangkan saat-saat kedatangan azab serta keadaan manusia waktu itu. Azab datang kepada orang kafir pada waktu langit hancur luluh, seperti perak yang mencair karena dipanaskan, dan pada saat gunung-gunung hancur bertaburan, seakan-akan bulu-bulu burung yang sedang beterbangan karena hembusan angin. Kebingungan dan penderitaan dihadapi manusia pada waktu itu. Masing-masing tidak dapat menolong orang lain, tidak seorang pun teman akrab yang menanyakan temannya, sedangkan mereka melihat dan mengetahui penderitaan temannya itu.",
        "intro_surah": "Surah ini terdiri dari 44 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-\u1e24\u0101qqah.\nPerkataan al-Ma\u2018\u0101rij yang menjadi nama surah ini adalah kata jamak dari kata mi\u2018raj, diambil dari kata al-ma\u2018\u0101rij yang terdapat pada ayat ke-3 surah ini, yang artinya menurut bahasa \u201ctempat naik\u201d. Sedangkan para mufasir memberi arti bermacam-macam, di antaranya ialah \u201clangit\u201d, karunia, dan derajat atau tingkatan yang diberikan Allah kepada penghuni surga.\n\nPokok-pokok Isinya:\nPerintah bersabar kepada Nabi Muhammad dalam menghadapi ejekan dan keingkaran orang-orang kafir; kejadian-kejadian pada hari kiamat; azab Allah tidak dapat dihindarkan dengan tebusan apa pun; sifat-sifat manusia yang mendorong mereka ke api neraka; amal perbuatan yang dapat membawa manusia ke martabat yang tinggi; peringatan Allah akan mengganti kaum yang durhaka dengan kaum yang lebih baik.",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tSurah al-Ma\u2018\u0101rij melengkapi Surah al-\u1e24\u0101qqah tentang gambaran hari Kiamat dan hari hisab.\n2. \tDalam Surah al-\u1e24\u0101qqah disebutkan dua golongan manusia pada hari Kiamat, yaitu ahli surga yang menerima kitab dari sebelah kanan, dan ahli neraka yang menerima kitab dari sebelah kiri; sedangkan Surah al-Ma\u2018\u0101rij menerangkan sifat-sifat kedua golongan itu. ",
        "munasabah_prev_theme": null,
        "theme_group": "PENGINGKARAN AKAN ADANYA HARI KIAMAT",
        "kosakata": "Kosakata:\n1.\tAl-Ma\u2018\u0101rij \u0627\u0644\u0652\u0645\u064e\u0639\u064e\u0627\u0631\u0650\u062c\u064f (al-Ma\u2018\u0101rij/70: 3)\nAl-Ma\u2018\u0101rij adalah bentuk jamak (plural) dari kata mi\u2018raj yang berasal dari kata \u2018araja-ya\u2018ruju yang berarti naik ke atas. Dengan demikian, mi\u2018raj adalah alat yang digunakan untuk naik. Mi\u2018raj adalah peristiwa naiknya Nabi Muhammad dari Masjidil Aqsa ke Sidratul Muntaha. \u2018Araja juga diartikan dengan bertempat tinggal sehingga a\u1e6d-\u1e6cab\u0101\u1e6dab\u0101\u2018\u012b dalam tafsirnya al-M\u012bz\u0101n memahami al-ma\u2018\u0101rij dengan maqam (derajat/tempat) para malaikat. Dalam Al-Qur\u2019an, kata ini dengan berbagai bentuk derivasinya terulang sebanyak 9 kali. Kesemuanya digunakan dalam arti naik, sinonim dengan lafal \u1e63u\u2018\u016bd dan antonim dari inz\u0101l (turun), kecuali dalam an-N\u016br/24: 61 dan al-Fat\u1e25/48: 17. Dalam kedua surat tersebut diungkapkan dengan kata al-a\u2018raj dalam arti orang yang pincang. Dinamakan demikian karena al-a\u2018raj cara berjalannya seperti seseorang sedang naik atau mendaki. \nDalam konteks ayat ini, Allah menjelaskan bahwa Dia adalah Pemilik tempat-tempat naik (al-ma\u2018\u0101rij), yaitu pemilik semua langit yang merupakan sumber kekuatan dan keputusan. Pelakunya adalah para malaikat dan r\u016b\u1e25 untuk menggambarkan betapa sulit dan jauh tempat itu, serta betapa agung Allah. Dari tempat tersebut, malaikat-malaikat dan r\u016b\u1e25 naik kepada-Nya dalam sehari yang kadarnya lima puluh ribu tahun dalam hitungan manusia. Para ulama mengartikan kata r\u016b\u1e25 di sini dengan Malaikat Jibril atau jiwa seorang mukmin yang dengan amal salehnya, ia naik kepada-Nya yakni ke tempat turunnya perintah Allah, atau ketinggian yang mampu dicapai makhluk masing-masing sesuai dengan maqam mereka di sisi Allah. \n2.\tLa\u1e93\u0101 \u0644\u064e\u0638\u064e\u0649 (al-Ma\u2018\u0101rij/70: 15)\nLa\u1e93\u0101 terambil dari kata la\u1e93iya-yal\u1e93\u0101-la\u1e93an, yang berarti api murni yang bergejolak. Sifat api ini sangat panas dan bisa membakar apa yang ada atau membakar dirinya sendiri jika tidak ada sesuatu yang dibakar. La\u1e93a menjadi sebuah nama untuk neraka Jahanam, karena sifat apinya yang bergejolak dan sangat panas. Kata la\u1e93\u0101 terulang hanya dua kali yaitu dalam ayat ini dan Surah al-Lail/92: 14 (faan\u017cartukum n\u0101ran tala\u1e93\u1e93\u0101).\nPada ayat ini, Allah menjelaskan tentang siksa yang akan dialami orang-orang kafir. Pada ayat sebelumnya digambarkan mengenai keadaan hari kiamat. Langit yang kelihatan luas dan kokoh akan terburai pada hari itu seperti luluhan perak. Gunung-gunung yang menancap kuat dan kukuh akan hancur seperti kapas yang beterbangan. Pada saat itu, tidak ada seorang pun yang bisa menjadi penyelamat walaupun teman akrab saat di dunia. Para pendurhaka itu bahkan berharap seandainya anak-anak, teman, istri, dan keturunannya bisa dijadikan sebagai tebusan, tentu akan mereka serahkan asal bisa selamat dari siksaan tersebut. Pada ayat ini, Allah menegaskan bahwa sekali-kali hal tersebut tidak bisa dilakukan, karena pada hari itu tidak ada tawar menawar dan negosiasi. Penyesalan tidak akan ada artinya sama sekali. Bahkan sebaliknya, mereka akan dimasukkan ke dalam neraka yang bergejolak, yang bisa mengelupaskan kulit kepala dan kulit tubuh mereka. Neraka ini disediakan bagi mereka yang membelakangi keimanan dan kebenaran serta yang berpaling dari ajakan Rasul, juga mereka yang mengumpulkan harta dan enggan untuk menafkahkannya. ",
        "sabab_nuzul": null,
        "conclusion": null
    }
}