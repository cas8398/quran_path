{
    "id": 4237,
    "surah_id": 41,
    "ayah": 19,
    "page": 478,
    "quarter_hizb": 48.5,
    "juz": 24,
    "manzil": 6,
    "arabic": "\u0648\u064e\u064a\u064e\u0648\u0652\u0645\u064e \u064a\u064f\u062d\u0652\u0634\u064e\u0631\u064f \u0627\u064e\u0639\u0652\u062f\u064e\u0627\u06e4\u0621\u064f \u0627\u0644\u0644\u0651\u0670\u0647\u0650 \u0627\u0650\u0644\u064e\u0649 \u0627\u0644\u0646\u0651\u064e\u0627\u0631\u0650 \u0641\u064e\u0647\u064f\u0645\u0652 \u064a\u064f\u0648\u0652\u0632\u064e\u0639\u064f\u0648\u0652\u0646\u064e",
    "latin": "Wa yauma yu\u1e25syaru a\u2018d\u0101'ull\u0101hi ilan-n\u0101ri fahum y\u016bza\u2018\u016bn(a).",
    "translation": "(Ingatlah) hari (ketika) musuh-musuh Allah digiring ke neraka, lalu mereka dipisah-pisahkan.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 41,
        "arabic": " \u0641\u0635\u0651\u0644\u062a",
        "latin": "Fu\u1e63\u1e63ilat",
        "transliteration": "Fussilat",
        "translation": "Dijelaskan",
        "num_ayah": 54,
        "page": 477,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Ayat-ayat sebelum ini berbicara tentang azab dan siksaan yang ditimpakan kepada kaum pendurhaka ketika mereka masih di dunia, dan mengisyaratkan bahwa siksaan di akhirat jauh lebih dahsyat dan menghinakan. Ayat-ayat berikut menjelaskan bagaimana penggambaran azab akhirat tersebut. Dan ingatkanlah kaum kafir Mekah itu, wahai Nabi Muhammad, bahwa pada hari Kiamat ketika mereka musuh-musuh Allah itu, seperti kaum \u2018Ad dan Samud, digiring dengan kasar dan tanpa belas kasihan oleh para malaikat ke dalam neraka lalu mereka dipisah-pisahkan.",
        "tahlili": "Ayat ini menerangkan perintah Allah kepada Nabi Muhammad agar menyampaikan kepada orang-orang yang ingkar itu keadaan mereka pada hari Kiamat nanti. Pada hari itu, semua musuh-musuh Allah dan orang-orang yang ingkar kepada-Nya akan dikumpulkan dalam neraka. Mereka dihalau ke dalamnya seperti orang menggiring dan menghalau binatang ternak. Tidak ada satu pun yang luput dan tertinggal di antara mereka.",
        "intro_surah": "Surah Fu\u1e63\u1e63ilat terdiri dari 54 ayat dan termasuk kelompok surah-surah Makkiyyah, diturunkan sesudah Surah G\u0101fir.\n Dinamai \u201cFu\u1e63\u1e63ilat\u201d karena ada hubungannya dengan perkataan \u201cFu\u1e63\u1e63ilat\u201d yang terdapat pada permulaan surah ini. Maksudnya adalah ayat-ayat diperinci dengan jelas tentang hukum-hukum, keimanan, janji dan ancaman, budi pekerti, kisah, dan sebagainya.\nDinamai juga dengan \u201c\u1e24\u0101 M\u012bm as-Sajdah\u201d karena surah ini dimulai dengan \u201c\u1e24\u0101 M\u012bm\u201d dan dalam surah ini terdapat ayat Sajdah.\n\nPokok-pokok Isinya:\n1. \tKeimanan:\nAl-Qur\u2019an dan sikap orang-orang musyrik terhadapnya; kejadian-kejadian langit dan bumi dan apa yang pada keduanya membuktikan adanya Allah. Semua yang terjadi dalam alam semesta tidak lepas dari pengetahuan Allah.\n2. \tLain-lain:\nHikmah penciptaan gunung-gunung; anggota tubuh tiap-tiap orang menjadi saksi terhadap dirinya pada hari Kiamat, azab yang ditimpakan kepada kaum \u2018Ad dan Samud; permohonan orang-orang kafir agar dikembalikan ke dunia untuk mengerjakan amal-amal saleh; berita gembira dari malaikat kepada orang-orang yang beriman; anjuran menghadapi orang-orang kafir secara baik-baik; ancaman terhadap orang-orang yang mengingkari keesaan Allah, sifat-sifat Al-Qur\u2019an Al-Karim; manusia dan wataknya.\n",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH G\u0100FIR DENGAN\nSURAH FU\u1e62\u1e62ILAT\n\n1. \tKedua surah memberikan peringatan kepada orang-orang musyrik Mekah yang mengingkari Muhammad saw.\n2. \tKeduanya dimulai dengan menyebut sifat-sifat Al-Qur\u2019an.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu diterangkan akibat-akibat yang diterima orang-orang kafir di dunia, berupa malapetaka dan kesengsaraan seperti yang dialami oleh kaum \u2018Ad dan Samud. Pada ayat-ayat berikut ini diterangkan keadaan mereka di akhirat nanti. Mereka akan diadili di hadapan pengadilan Tuhan dengan seadil-adilnya, di mana anggota tubuh mereka sendiri akan menjadi saksi terhadap semua perbuatan yang telah mereka lakukan, sehingga tidak ada perbuatan mereka yang tidak diungkapkan. Allah Maha Mengetahui segala apa yang mereka kerjakan.\n",
        "theme_group": "\nKESAKSIAN ANGGOTA TUBUH DI AKHIRAT",
        "kosakata": "1. \tAn\u1e6daqan\u0101 \u0627\u064e\u0646\u0652\u0637\u064e\u0642\u064e\u0646\u064e\u0627 (Fu\u1e63\u1e63ilat/41: 21)\nAn\u1e6daqa berasal dari an-nu\u1e6dq yang artinya bersuara. Sedangkan an\u1e6daqa artinya membuat sesuatu berbicara. Kata an-nu\u1e6dq hanya digunakan untuk manusia, tetapi dalam ayat ini digambarkan kulit dapat menyampaikan kesaksian yang bisa berarti makna kecaman. Kulit disebutkan dalam Al-Qur\u2019an sebagai saksi selain tangan dan kaki karena kulit mewakili seluruh jasmani manusia. Sehingga jika manusia dimasukkan ke dalam api neraka, kulitlah yang tersiksa karena kulit memiliki indra perasa. Dalam Surah al-M\u0101'idah/5 ayat 56 disebutkan bagaimana Allah memperbaharui kulit orang-orang yang durhaka ketika kulit mereka sudah habis terbakar agar si pemilik kulit terus merasakan pedihnya siksaan.\n2. \tArd\u0101kum \u0627\u064e\u0631\u0652\u062f\u064e\u0627\u0643\u064f\u0645\u0652 (Fu\u1e63\u1e63ilat/41: 23)\nAsal katanya adalah arrada yang artinya kebinasaan. Yang dimaksud dengan kata ard\u0101kum dalam ayat di atas adalah mengakibatkan mereka berada dalam keadaan atau kondisi yang sangat buruk, tidak berdaya seperti orang yang sudah mati.\n3. \tAl-Mu\u2018tab\u012bn \u0627\u0644\u0652\u0645\u064f\u0639\u0652\u062a\u064e\u0628\u0650\u064a\u0652\u0646\u064e (Fu\u1e63\u1e63ilat/41: 24)\nAl-Mu\u2018tab\u012bn berasal dari kata al-\u2018itb yang bermakna kecaman, ungkapan ketidaksenangan, keluhan atas kesalahan orang lain, dan lain-lain. Dari kata ini, lahir kata \u2018\u0101taba yang menggambarkan upaya seseorang menyampaikan kesalahan temannya, namun ia menunjukkan kesediaan untuk memaafkan-nya. Al-Mu\u2018tab\u012bn berarti orang-orang yang tidak diterima Allah keluhan-keluhannya atau kecaman-kecamannya agar mereka dimaafkan Allah. Ayat ini menginformasikan bahwa orang-orang yang durhaka tersebut tidak bisa diterima keluhan-keluhannya dengan tujuan untuk dimaafkan kesalahan-kesalahannya.\n",
        "sabab_nuzul": null,
        "conclusion": null
    }
}