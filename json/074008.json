{
    "id": 5503,
    "surah_id": 74,
    "ayah": 8,
    "page": 575,
    "quarter_hizb": 58.25,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u0641\u064e\u0627\u0650\u0630\u064e\u0627 \u0646\u064f\u0642\u0650\u0631\u064e \u0641\u0650\u0649 \u0627\u0644\u0646\u0651\u064e\u0627\u0642\u064f\u0648\u0652\u0631\u0650\u06d9 ",
    "latin": "Fa i\u017c\u0101 nuqira fin-n\u0101q\u016br(i).",
    "translation": "Apabila sangkakala ditiup,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 74,
        "arabic": " \u0627\u0644\u0645\u062f\u0651\u062b\u0651\u0631",
        "latin": "Al-Mudda\u1e61\u1e61ir",
        "transliteration": "Al-Muddassir",
        "translation": "Orang Berselimut",
        "num_ayah": 56,
        "page": 575,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Kesulitan dalam dakwah tidaklah seberapa, akan ada saat yang lebih sulit lagi, maka apabila sangkakala ditiup yaitu hari Kiamat telah tiba,",
        "tahlili": "Setelah memberikan pengarahan khusus kepada Nabi Muhammad (yang juga menjadi cermin pengajaran bagi umat beliau) yang dimulai dari ayat 1 sampai dengan ayat 7 di atas, maka pada ayat ini, Allah menjelaskan pula tentang suasana kedatangan hari Kiamat. Di hari yang dijanjikan itu, orang-orang yang telah menyakiti hati para rasul dan juru dakwah karena menyampaikan ajaran Allah, akan mengalami suatu kesulitan yang luar biasa. Mereka tersentak mendengar seruan Kiamat ditiup Malaikat Israfil. Mereka langsung merasakan betapa hebatnya kesulitan yang harus ditempuh. Oleh karena itu, Allah memerintahkan Nabi Muhammad supaya bersabar menghadapi gangguan-gangguan musuh tersebut.\nPada hari Kiamat, semua orang mendapatkan apa yang telah mereka amalkan: kesenangan yang abadi bagi orang yang beriman dan berjihad menegakkan keimanan yang benar, serta kecelakaan dan kesengsaraan bagi siapa yang ingkar dan hidup di atas keingkaran itu.",
        "intro_surah": "Surah al-Mudda\u1e61\u1e61ir terdiri 56 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-Muzzammil. \nNama al-Mudda\u1e61\u1e61ir diambil dari perkataan al-mudda\u1e61\u1e61ir yang terdapat pada ayat pertama surah ini.\n\nPokok-pokok Isinya:\nPerintah untuk mulai berdakwah mengagungkan Allah, membersihkan pakaian, menjauhi maksiat, memberikan sesuatu dengan ikhlas, dan bersabar dalam menjalankan perintah serta menjauhi larangan Allah; Allah akan mengazab orang-orang yang menentang Nabi Muhammad dan mendustakan Al-Qur\u2019an; tiap-tiap manusia terikat dengan apa yang telah diusahakannya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tKedua surah ini sama-sama dimulai dengan seruan kepada Nabi Muhammad.\n2.\tSurah al-Muzzammil berisi perintah bangun di malam hari untuk melakukan salat Tahajud dan membaca Al-Qur\u2019an untuk menguatkan jiwa seseorang, sedangkan Surah al-Mudda\u1e61\u1e61ir berisi perintah melakukan dakwah menyucikan diri dan bersabar.",
        "munasabah_prev_theme": "Pada akhir Surah al-Muzzammil, Nabi Muhammad diperintahkan agar menjadikan Allah sebagai pelindung. Pada awal Surah al-Mudda\u1e61\u1e61ir, Nabi Muhammad diperintahkan untuk berdakwah.",
        "theme_group": "PERINTAH KEPADA NABI UNTUK BERDAKWAH",
        "kosakata": "Kosakata:\n1.\tAl-Mudda\u1e61\u1e61ir  \u0627\u0644\u0652\u0645\u064f\u062f\u0651\u064e\u062b\u0651\u0650\u0631  (al-Mudda\u1e61\u1e61ir/74: 1)\n\tKata al-mudda\u1e61\u1e61ir adalah isim f\u0101\u2018il dari tada\u1e61\u1e61ara. Menurut al-Ragib al-A\u1e63fahan\u012b, kata mudda\u1e61\u1e61ir berasal dari kata  mutada\u1e61\u1e61ir, di-idg\u0101m-kan menjadi dal. Sedangkan menurut  pengarang al-Mu\u2018jam al-Wasi\u1e6d,  kata tada\u1e61\u1e61ara berarti seseorang yang memakai di\u1e61\u0101r, yaitu sejenis kain yang diletakkan di atas baju yang dipakai untuk menghangatkan atau dipakai sewaktu orang berbaring atau tidur. Oleh sebab itu, kata di\u1e61\u0101r dapat diartikan dengan \u201cselimut\u201d. Dengan demikian, maka  kata al-mudda\u1e61\u1e61ir berarti \u201corang yang berselimut\u201d. Ulama tafsir sepakat bahwa yang dimaksud dengan yang berselimut adalah Nabi Muhammad. Makna ini dapat dipahami dari sabab nuzul ayat yang berkenaan dengan pembahasan ini. Pengertian ini didukung oleh qira\u2018ah atau bacaan yang dinisbahkan  kepada \u2018Ikrimah, yaitu \u201cy\u0101 ayyuhal-mud\u1e61ar\u201d.\n\tBiasanya bila seseorang takut, ia akan menggigil, oleh sebab itu ia menutupi dirinya dengan selimut. Menyelimuti atau diselimuti dalam ayat tersebut adalah untuk menghilangkan rasa takut yang meliputi jiwa Nabi Muhammad beberapa saat sebelum turunnya ayat-ayat ini. Hal ini terjadi pada  diri Nabi Muhammad, khususnya pada masa awal kedatangan malaikat Jibril  kepada beliau. \n2.\tAn-n\u0101q\u016br \u0627\u0644\u0646\u0651\u064e\u0627\u0642\u064f\u0648\u0652\u0631  (al-Mudda\u1e61\u1e61ir/74: 8)\n\tKata an-n\u0101q\u016br terambil dari kata naqara-yanquru-naqran. Kalimat naqara\u1e6d-\u1e6d\u0101'irusy-syai'a berarti burung itu melubangi sesuatu. Dari kata ini diambil kata naq\u012br yang berarti lubang yang ada pada bagian atas biji-bijian. Darinya juga diambil kata n\u0101q\u016br yang berarti sangkakala yang ditiup oleh malaikat. Kalimat nuqira fin-n\u0101q\u016br berarti sangkakala ditiup.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}