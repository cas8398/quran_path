{
    "id": 6081,
    "surah_id": 93,
    "ayah": 2,
    "page": 596,
    "quarter_hizb": 60.25,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0648\u064e\u0627\u0644\u0651\u064e\u064a\u0652\u0644\u0650 \u0627\u0650\u0630\u064e\u0627 \u0633\u064e\u062c\u0670\u0649\u06d9",
    "latin": "Wal-laili i\u017c\u0101 saj\u0101.",
    "translation": "dan demi waktu malam apabila telah sunyi,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 93,
        "arabic": "\u0627\u0644\u0636\u0651\u062d\u0649",
        "latin": "A\u1e0d-\u1e0cu\u1e25\u0101",
        "transliteration": "Ad-Duha",
        "translation": "Duha",
        "num_ayah": 11,
        "page": 596,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Dan demi malam apabila telah sunyi dan gelap. Ketika matahari bergeser ke tempat lain, belahan bumi yang ditinggalkannya beranjak tenang dan gelap, menjadi waktu yang tepat untuk istirahat.",
        "tahlili": "Dalam ayat-ayat ini, Allah bersumpah dengan dua macam tanda-tanda kebesaran-Nya, yaitu \u1e0cu\u1e25\u0101 (waktu matahari naik sepenggalah) bersama cahayanya dan malam beserta kegelapan dan kesunyiannya, bahwa Dia tidak meninggalkan Rasul-Nya, Muhammad, dan tidak pula memarahinya, sebagaimana orang-orang mengatakannya atau perasaan Rasulullah sendiri.",
        "intro_surah": "Surah a\u1e0d-\u1e0cu\u1e25\u0101 terdiri dari 11 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah surah al-Fajr. Nama a\u1e0d-\u1e0cu\u1e25\u0101 diambil dari kata a\u1e0d-\u1e0cu\u1e25\u0101 yang terdapat pada ayat pertama, artinya, \u201cketika matahari naik sepenggalah\u201d.\n\nPokok-pokok Isinya:\nSurah ini berisi bantahan terhadap persangkaan kaum musyrikin Mekah dulu bahwa Allah meninggalkan Nabi Muhammad dan membenci beliau. Masalah kapan dan kepada siapa Allah menurunkan wahyu-Nya, itu adalah wewenang-Nya sepenuhnya, manusia tidak dapat campur tangan. Di samping bantahan itu, Allah juga menyampaikan nikmat-nikmat yang telah diberikan-Nya kepada Nabi Muhammad, supaya beliau berbesar hati. Allah kemudian memberikan perintah-perintah-Nya untuk beliau laksanakan, khususnya perhatian pada anak yatim dan orang miskin.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-LAIL\nDENGAN SURAH A\u1e0c-\u1e0cU\u1e24\u0100\n\nSetelah pada surah sebelumnya (al-Lail), diceritakan tentang dua kelompok manusia, yang bahagia dan celaka. Terhadap orang yang bertakwa, yang selalu berinfak karena Allah, dia akan mendapatkan rida dari Allah swt. Maka pada Surah a\u1e0d-\u1e0cu\u1e25a Allah menjelaskan bahwa Nabi Muhammad saw orang yang paling berbahagia di dunia dan juga kelak di akhirat. Allah tidak akan meninggalkannya dan Allah akan memberikan karunia yang agung dan akan meridainya.",
        "munasabah_prev_theme": "Pada akhir surah yang lalu dijelaskan bahwa Allah akan menjauhkan orang-orang yang bertakwa dari api neraka. Mereka akan memperoleh kepuasan dan rida Allah. Pada ayat-ayat berikut ini ditegaskan kembali janji Allah tersebut, yaitu Dia pasti akan memberi karunia dan menjadikan mereka puas.",
        "theme_group": "BEBERAPA NIKMAT YANG DIANUGERAHKAN\nKEPADA NABI MUHAMMAD",
        "kosakata": "Kosakata:\n1.\tSaj\u0101 \u0633\u064e\u062c\u064e\u0649 (a\u1e0d-\u1e0cu\u1e25\u0101/93: 2)\nKata saj\u0101 merupakan bentuk fi\u2018il m\u0101\u1e0d\u012b yang memiliki arti tenang, tidak bergerak. Saj\u0101 al-ba\u1e25r berarti laut itu ombaknya tenang, tidak berbahaya. Mata yang sayu dan sendu disebut dengan s\u0101jiyah. Unta yang telah diperah susunya dan duduk dengan tenang dinamai sajw\u0101. Tasjiyah adalah menutup jenazah dengan kain kafan. Dari beberapa pengertian di atas, kata saj\u0101 mengandung makna ketenangan dan kenyamanan. Kata ini hanya terulang sekali yaitu dalam ayat ini.\nPada ayat ini, Allah menjelaskan tentang sumpah-Nya yaitu demi waktu \u1e0cu\u1e25\u0101 dan demi malam apabila telah sunyi. Kata al-lail adalah waktu antara tenggelam matahari sampai terbit fajar. Keadaan malam dari sisi kegelapannya berbeda dari satu saat ke saat yang lain. Untuk menggambarkan keadaan malam ini, maka Allah mengungkapkannya dengan saj\u0101 yang berarti hening, tenang, tidak ada suara bising atau gaduh. Inilah sifat malam yang menjadi waktu istirahat manusia setelah merasa lelah bekerja pada siang hari.\n2.\tQal\u0101 \u0642\u064e\u0644\u064e\u0649 (a\u1e0d-\u1e0cu\u1e25\u0101/93: 3)\nKata qal\u0101 terambil dari kata al-qa-w (dengan w\u0101u) yang berarti pelemparan. Seseorang atau sesuatu yang menjadi objek penderita dari kata tersebut (yang dilemparkan), seakan-akan dilempar keluar dari hati akibat kebencian si pelempar terhadap yang bersangkutan. Qalatin-n\u0101qah bir\u0101kibih\u0101 artinya unta itu melemparkan penumpangnya. Dari sini kata tersebut diartikan sebagai kebencian yang telah mencapai puncaknya. Sebagian mengatakan berasal dari kata al-qalyu (dengan y\u0101\u2019) yang berarti menggoreng atau memasak. Dari dua pengertian di atas, makna pertama lebih cocok yaitu menggambarkan kebencian yang amat sangat atau kebencian yang sudah mencapai tingkat akumulasinya. Lafal ini hanya ditemukan dua kali dalam Al-Qur\u2019an yaitu pada ayat ini dan Surah asy-Syu\u2018ar\u0101\u2019/26: 168 (Inn\u012b li\u2018amalikum minal-q\u0101l\u012bn) yang berbicara tentang ajakan Nabi Lut kepada kaumnya untuk meninggalkan kebiasaan yang sangat dibenci yaitu perilaku homoseksual. Tidak disebutkannya objek dalam ayat ini menunjukkan bahwa tidak hanya Muhammad yang tidak dibenci oleh Allah, tetapi juga kaumnya dan Dia tidak membenci dengan kebencian yang amat sangat kepada siapa pun.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}