{
    "id": 5801,
    "surah_id": 81,
    "ayah": 1,
    "page": 586,
    "quarter_hizb": 59.25,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0627\u0650\u0630\u064e\u0627 \u0627\u0644\u0634\u0651\u064e\u0645\u0652\u0633\u064f \u0643\u064f\u0648\u0651\u0650\u0631\u064e\u062a\u0652\u06d6",
    "latin": "I\u017casy-syamsu kuwwirat.",
    "translation": "Apabila matahari digulung,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 81,
        "arabic": "\u0627\u0644\u062a\u0651\u0643\u0648\u064a\u0631",
        "latin": "At-Takw\u012br",
        "transliteration": "At-Takwir",
        "translation": "Penggulungan",
        "num_ayah": 29,
        "page": 586,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Allah mengawali surah ini dengan menyebutkan dua belas peristiwa besar yang akan terjadi pada hari kiama  disebutkan dari ayat 1 s.d. 13. Apabila matahari yang demikian besar digulung dengan mudah seperti halnya serban, hingga cahayanya memudar dan redup.",
        "tahlili": "Dalam ayat ini, Allah menerangkan bahwa jika matahari telah digulung, telah padam cahayanya dan jatuh berantakan bersamaan dengan hancurnya alam semesta yang pernah didiami oleh makhluk-makhluk yang hidup di dunia, maka musnahlah segala alam karena berpindah kepada alam yang lain.",
        "intro_surah": "Surah at-Takw\u012br terdiri dari 29 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-Lahab.\nKata at-Takw\u012br yang menjadi nama bagi surah ini adalah kata asal (ma\u1e63dar) dari kata kerja kuwwirat (digulung) yang terdapat pada ayat pertama surah ini.\n\nPokok-pokok Isinya:\nKeguncangan-keguncangan yang terjadi pada hari Kiamat; pada hari Kiamat setiap jiwa akan mengetahui apa yang telah dikerjakannya waktu di dunia; Al-Qur\u2019an adalah firman Allah yang disampaikan oleh Jibril a.s.; penegasan atas kenabian Muhammad saw; Al-Qur\u2019an sumber petunjuk bagi umat manusia yang menginginkan hidup lurus; suksesnya manusia dalam mencatat kehidupan yang lurus itu tergantung kepada taufik dari Allah.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tSama-sama menerangkan tentang huru-hara pada hari Kiamat.\n2.\tSama-sama menerangkan bahwa manusia pada hari Kiamat terbagi dua.\n3.\tPada Surah \u2018Abasa, Allah menegur Muhammad saw, sedang dalam at-Takw\u012br, Allah menegaskan bahwa Muhammad saw adalah seorang rasul yang mulia.",
        "munasabah_prev_theme": "Pada akhir Surah \u2018Abasa dijelaskan situasi dan keadaan hari Kiamat, di mana semua manusia sibuk dengan urusan mereka masing-masing karena dahsyatnya gejala-gejala alam yang mengiringinya. Masing-masing menyikapi hari Kiamat sesuai dengan amal perbuatan mereka. Orang-orang mukmin tertawa gembira, sedangkan orang-orang kafir wajah mereka menjadi kelam karena ketakutan dan kesedihan. Pada awal Surah at-Takw\u012br, Allah bersumpah dengan berbagai makhluk-Nya seperti matahari yang digulung, bintang-bintang yang berjatuhan, gunung-gunung yang dihancurkan, dan unta-unta bunting yang tidak dipedulikan lagi dan sebagainya. Tujuan sumpah itu adalah memberitahu manusia bahwa di hari Kiamat manusia akan mengetahui semua amal perbuatan mereka di dunia dari buku catatan amal mereka.",
        "theme_group": "PERISTIWA-PERISTIWA BESAR\nPADA HARI KIAMAT",
        "kosakata": "Kosakata:\n1.\tKuwwirat \u0643\u064f\u0648\u0651\u0650\u0631\u064e\u062a\u0652 (at-Takw\u012br/81: 1)\nKata kuwwirat adalah fi\u2018il m\u0101\u1e0d\u012b mabn\u012b majh\u016bl yaitu kata kerja untuk waktu lampau dalam bentuk pasif. Tetapi fi\u2018il m\u0101\u1e0d\u012b dalam Al-Qur\u2019an bukan hanya berarti untuk waktu lampau, tetapi juga berarti taukid yaitu betul-betul terjadi. Ayat 1 yang berbunyi i\u017c\u0101sy-syamsu kuwwirat artinya: jika matahari betul-betul telah digulung. Dalam ungkapan sehari-hari: huwa kawwaral-\u2018im\u0101mah, artinya dia melilitkan atau melingkarkan sorbannya di kepala. Ayat-ayat pada permulaan Surah at-Takw\u012br ini menggambarkan keadaan dahsyat pada hari Kiamat, yaitu matahari digulung sehingga menjadi padam seperti masuk dalam lipatan awan, bintang-bintang pun hilang cahayanya, dan hancurlah alam semesta ini. Gunung-gunung menjadi hancur berantakan, laut pun dipanaskan oleh perut bumi dan memancarkan airnya yang bercampur api. Keadaannya sangat dahsyat dan sungguh mengerikan, peristiwa yang luar biasa.\n2.\tAl-Wu\u1e25\u016bsy \u0627\u064e\u0644\u0652\u0648\u064f\u062d\u064f\u0648\u0652\u0634\u064f (at-Takw\u012br/81: 5)\nKata al-wu\u1e25\u016bsy adalah bentuk jamak dari wa\u1e25sy artinya binatang liar atau binatang buas. Ayat 5 masih dalam rangkaian menerangkan hari Kiamat, keadaan yang kacau balau, gunung-gunung hancur beterbangan, sampai-sampai unta yang bunting pun ditinggalkan. Padahal kebiasaan orang Arab sangat memperhatikan unta yang sedang bunting. Orang-orang menjadi sangat tidak peduli karena bingung memikirkan diri masing-masing. Dalam keadaan demikian, binatang-binatang buas dan liar pun dikumpulkan sehingga menambah rasa takut yang sudah bertumpuk-tumpuk. Bukan hanya binatang buas yang besar-besar seperti banteng, harimau, gajah, singa, badak, dan lain-lain, juga binatang liar yang kecil-kecil seperti ular berbisa, kelabang, kalajengking, dan lain-lain. Sungguh keadaan hari kiamat sangat mencekam sehingga teringatlah setiap orang akan dosa-dosa yang telah dilakukan, yang menimbulkan penyesalan yang sangat besar. Akan tetapi, penyesalan yang datang terlambat ini sudah tidak ada lagi gunanya, hari Kiamat telah datang dan kehidupan dunia sudah berakhir.\n3.\tAl-Mau\u2019\u016bdah \u0627\u064e\u0644\u0652\u0645\u064e\u0648\u0652\u0621\u064f\u0648\u0652\u062f\u064e\u0629\u064f (at-Takw\u012br/81: 8)\nKata al-mau\u2019\u016bdah adalah isim maf\u2018\u016bl atau orang yang menjadi objek dari fi\u2018il wa\u2019ada-ya\u2019idu-wa\u2019dan yang artinya mengubur hidup-hidup. Kebiasaan sebagian orang Arab Jahiliah yang disebut wa\u2019dul-ban\u0101t artinya mengubur hidup-hidup bayi perempuan mereka. Kata al-mau'\u016bdah artinya bayi-bayi perempuan yang dikubur hidup-hidup. Ayat 8 ini masih dalam rangkaian gambaran peristiwa-peristiwa yang terjadi pada hari Kiamat dan hari kebangkitan yaitu bagaimana keadaan bayi-bayi perempuan yang mereka kubur hidup-hidup, dosa apa gerangan sehingga bayi-bayi itu dikubur hidup-hidup. Orang-orang tua bayi-bayi itulah yang sangat besar dosanya mengubur hidup-hidup bayi-bayi mereka, hanya karena bayi-bayi itu lahir berjenis kelamin perempuan dan orang-orang Arab Jahiliah tidak suka pada anak perempuan. Padahal bayi-bayi itu tidak berdosa, baru saja lahir dalam keadaan suci, baru mau menghirup udara kehidupan dunia tetapi langsung dikubur hidup-hidup.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}