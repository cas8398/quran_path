{
    "id": 5309,
    "surah_id": 68,
    "ayah": 38,
    "page": 565,
    "quarter_hizb": 57.25,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u0627\u0650\u0646\u0651\u064e \u0644\u064e\u0643\u064f\u0645\u0652 \u0641\u0650\u064a\u0652\u0647\u0650 \u0644\u064e\u0645\u064e\u0627 \u062a\u064e\u062e\u064e\u064a\u0651\u064e\u0631\u064f\u0648\u0652\u0646\u064e\u06da",
    "latin": "Inna lakum f\u012bhi lam\u0101 takhayyar\u016bn(a).",
    "translation": "Sesungguhnya di dalamnya kamu dapat memilih apa saja yang kamu sukai.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 68,
        "arabic": " \u0627\u0644\u0642\u0644\u0645",
        "latin": "Al-Qalam",
        "transliteration": "Al-Qalam",
        "translation": "Pena",
        "num_ayah": 52,
        "page": 564,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "sehingga menemukan ketentuan bahwa sesungguhnya kamu dapat memilih apa saja yang ada di dalamnya?",
        "tahlili": "Dalam ayat ini, dinyatakan bahwa pendapat atau jalan pikiran orang-orang kafir itu tidak berdasarkan wahyu dari Allah. Tidak ada satu pun dari kitab Allah yang menerangkan hal yang demikian itu. Ungkapan itu dilontarkan kepada mereka dalam bentuk pertanyaan, \u201cApakah kamu, hai orang-orang kafir, mempunyai suatu kitab yang diturunkan dari langit, yang kamu terima dari nenek moyangmu kemudian kamu pelajari secara turun-temurun, yang mengandung suatu ketentuan seperti yang kamu katakan itu. Apakah kamu memiliki kitab yang semacam itu yang membolehkan kamu memilih apa yang kamu inginkan sesuai dengan kehendakmu.\u201d\nAyat ini dikemukakan dalam bentuk kalimat tanya. Biasanya kalimat tanya bermaksud untuk menanyakan sesuatu yang tidak diketahui, tetapi kalimat tanya di sini untuk mengingkari dan menyatakan kejelekan suatu perbuatan. Seakan-akan Allah menyatakan kepada orang-orang kafir bahwa tidak ada suatu pun wahyu-Nya yang menyatakan demikian. Ucapan mereka itu adalah ucapan yang mereka ada-adakan dan cara mengada-adakan yang demikian itu adalah cara yang tidak terpuji.",
        "intro_surah": "Surah ini terdiri dari 52 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-\u2018Alaq.\nNama al-Qalam yang artinya qalam atau pena, diambil dari kata al-qalam yang terdapat pada ayat pertama surah ini. Surah ini dinamai pula dengan \u201cN\u016bn\u201d (huruf nun), yang diambil dari huruf pertama yang terdapat pada permulaan ayat-ayat surah ini.\n\nPokok-pokok Isinya:\nNabi Muhammad bukanlah orang yang gila, melainkan manusia yang berbudi pekerti yang agung; larangan bertoleransi dalam bidang kepercayaan; larangan mengikuti sifat-sifat orang yang dicela Allah; nasib yang dialami orang-orang yang tidak bersyukur terhadap nikmat Allah; kecaman-kecaman Allah kepada mereka yang ingkar dan azab yang akan menimpa mereka; Al-Qur\u2019an adalah peringatan bagi seluruh umat.",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tPada akhir Surah al-Mulk, Allah mengancam orang-orang yang tidak bersyukur kepada-Nya dengan mengeringkan bumi, sedang dalam Surah al-Qalam diberi contoh azab yang ditimpakan kepada orang-orang yang tidak bersyukur kepada-Nya.\n2. \tKedua surah ini sama-sama menerangkan ancaman yang diberikan kepada orang-orang kafir.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menerangkan bahwa segala sesuatu yang dialami adalah cobaan bagi manusia, baik berupa kesengsaraan maupun berupa kesenangan. Dengan cobaan itu, Allah hendak mengetahui apakah harta yang dikaruniakan kepada mereka dijadikan untuk kepentingan maksiat, sehingga Allah mengazab mereka, atau untuk kepentingan yang sesuai dengan agama-Nya. Pada ayat-ayat berikut ini, Allah menerangkan bahwa orang-orang yang bertakwa dan taat kepada-Nya akan memperoleh surga dan mereka kekal di dalamnya. Allah membantah anggapan orang-orang kafir yang mengatakan bahwa mereka lebih baik keadaannya dari umat Islam di akhirat, karena keadaan mereka di dunia lebih baik dari orang Islam. Allah membantah anggapan orang-orang kafir dengan mengatakan bahwa sekali-kali tidak sama orang-orang mukmin dengan orang-orang kafir.",
        "theme_group": "ALLAH TIDAK MENYAMAKAN ORANG KAFIR\nDENGAN ORANG MUKMIN",
        "kosakata": "Kosakata: \n1.\tTadrus\u016bn \u062a\u064e\u062f\u0652\u0631\u064f\u0633\u064f\u0648\u0652\u0646\u064e (al-Qalam/68: 37)\nTadrus\u016bn berarti mempelajari atau meneliti sesuatu guna diambil manfaatnya. Dalam konteks ayat ini, tadrus\u016bn adalah membahas dan mendiskusikan kitab suci untuk mengambil informasi dan pesan-pesan yang dikandungnya. Pertanyaan menyangkut adanya kitab suci yang mereka baca dan pelajari merupakan sindiran terhadap orang-orang musyrik Mekah karena seandainya mereka memiliki kitab suci, mereka juga tidak bisa membacanya karena kebanyakan dari mereka buta huruf. \n2.\tTakhayyar\u016bn \u062a\u064e\u062e\u064e\u064a\u0651\u064e\u0631\u064f\u0648\u0652\u0646\u064e (al-Qalam/68: 38)\nTakhayyar\u016bn berasal dari kata al-khair, yaitu segala sesuatu yang diinginkan. Akar katanya adalah \u201ckha-ya-ra\u201d yang artinya menyenangi atau cenderung. Al-Khair adalah lawan dari asy-syar yaitu kejahatan. Dari sini muncul kata khayyara yang berarti memilih. Ayat ini mempertanyakan kepada orang-orang musyrik Mekah apakah mereka memiliki kitab suci yang di dalamnya mereka bisa atau boleh memilih secara sungguh-sungguh apa yang mereka sukai. Juga dijelaskan tentang ganjaran yang diancamkan Allah kepada mereka.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}