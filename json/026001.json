{
    "id": 2933,
    "surah_id": 26,
    "ayah": 1,
    "page": 367,
    "quarter_hizb": 37.5,
    "juz": 19,
    "manzil": 5,
    "arabic": "\u0637\u0670\u0633\u06e4\u0645\u0651\u06e4 ",
    "latin": "\u1e6c\u0101 S\u012bm M\u012bm.",
    "translation": "\u1e6c\u0101 S\u012bn M\u012bm.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 26,
        "arabic": "\u0627\u0644\u0634\u0651\u0639\u0631\u0627\u06e4\u0621",
        "latin": "Asy-Syu\u2018ar\u0101'",
        "transliteration": "Asy-Syu\u2018ara'",
        "translation": "Para Penyair",
        "num_ayah": 227,
        "page": 367,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Ta Sin Mim. Nama-nama huruf yang dengannya Al-Qur\u2019an tersusun. Susunan Al-Qur\u2019an tidak mampu disaingi oleh manusia. Inilah yang menjadikan Al-Qur\u2019an menjadi mukjizat sepanjang zaman.",
        "tahlili": "Lihat keterangan ayat ini pada jilid I yang menerangkan tentang faw\u0101ti\u1e25us-suwar (al-Baqarah/2: 1).",
        "intro_surah": "Surah ini terdiri dari 227 ayat, termasuk kelompok surah-surah Makkiyyah. Dinamakan asy-Syu\u2019ar\u0101\u2019 (kata jamak dari asy-sy\u0101\u2019ir yang berarti penyair) diambil dari kata \u201casy-Syu\u2019ar\u0101\u2019\u201d yang terdapat pada ayat 224, yaitu pada bagian terakhir surah ini, ketika Allah swt secara khusus menyebutkan kedudukan penyair-penyair. Para penyair itu mempunyai sifat-sifat yang jauh berbeda dengan rasul-rasul. Mereka diikuti oleh orang-orang yang sesat dan suka memutarbalikkan lidah serta tidak mempunyai pendirian, per-buatannya tidak sesuai dengan apa yang diucapkan. Sifat-sifat yang demikian itu tidaklah sekali-kali dimiliki para rasul. Oleh karena itu, tidak patut bila Nabi Muhammad dituduh sebagai penyair dan Al-Qur\u2019an dianggap sebagai syair. Al-Qur\u2019an adalah wahyu Allah bukan buatan manusia.\n\nPokok-pokok Isinya:\n1. \tKeimanan:\nJaminan Allah akan kemenangan perjuangan dan keselamatan para rasul-Nya. Al-Qur\u2019an benar-benar wahyu Allah yang diturunkan ke dunia melalui Malaikat Jibril a.s. (R\u016b\u1e25ul-Am\u012bn); hanya Allah yang wajib disembah.\n2. \tHukum-hukum:\nKeharusan menyempurnakan takaran dan timbangan, larangan mengubah syair yang berisi caci maki, khurafat, dan kebohongan.\n3. \tKisah-kisah:\nKisah Nabi Musa dengan Fir\u2018aun, Kisah Nabi Ibrahim, Nabi Nuh, Nabi Hud, Nabi Saleh, dan Nabi Lut dengan kaum mereka masing-masing; serta kisah Nabi Syuaib dengan penduduk Aikah.\n4. \tLain-lain:\nKebinasaan suatu bangsa/umat karena meninggalkan petunjuk-petunjuk agama, tumbuh-tumbuhan yang beraneka ragam dan perubahan-perubahan yang terjadi atasnya adalah bukti kekuasaan Tuhan Yang Maha Esa. Petunjuk-petunjuk Allah bagi para pemimpin agar berlaku lemah-lembut terhadap pengikut mereka. Al-Qur\u2019an turun dalam bahasa Arab sebagaimana disebutkan dalam kitab-kitab suci terdahulu.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-FURQ\u0100N DENGAN\nSURAH ASY-SYU\u2019AR\u0100\u2019\n\n1. \tBeberapa persoalan dalam Surah al-Furq\u0101n diuraikan kembali secara luas dalam Surah asy-Syu\u2019ar\u0101\u2019 antara lain kisah para nabi.\n2. \tMasing-masing dari kedua surah itu dimulai dengan keterangan dari Allah bahwa Al-Qur\u2019an adalah petunjuk bagi manusia dan pembeda antara yang hak dengan yang batil dan ditutup dengan ancaman bagi orang yang mendustakan Allah.\n",
        "munasabah_prev_theme": "Pada akhir Surah al-Furq\u0101n telah dijelaskan sifat-sifat orang mukmin yang baik sehingga mendapat predikat hamba-hamba Allah Yang Maha Pemurah. Juga dijelaskan tentang sikap orang kafir yang mendustakan Nabi. Allah mengancam mereka dengan azab yang abadi di neraka Jahanam. Pada awal Surah asy-Syu\u2019ar\u0101\u2019 ini, Allah menjelaskan tentang Al-Qur\u2019an sebagai kitab suci yang jelas, dan sikap Rasul yang begitu ingin agar kaumnya beriman, sehingga hampir membinasakan dirinya karena banyaknya orang yang menolak beriman kepada Allah, Al-Qur\u2019an, dan kenabian.",
        "theme_group": "NABI MUHAMMAD SAW TIDAK PERLU \nBERSEDIH HATI ATAS KEINGKARAN KAUM MUSYRIKIN",
        "kosakata": "Kosakata: B\u0101khi\u2019 \u0628\u064e\u0627\u062e\u0650\u0639\u064c (asy-Syu\u2019ar\u0101\u2019/26: 3)\nBerasal dari kata kerja bakha\u2019a yang berarti menyembelih. Kata ini terambil dari kata bukha\u2019 yaitu urat nadi yang terdapat di bagian belakang binatang. Jika urat nadi ini dipotong, maka nyawa terpisah dari badan. Kata ini menggambarkan kesedihan yang luar biasa hingga bisa menyebabkan kematian. \nDalam ayat ini, Allah menjelaskan bagaimana keadaan Nabi yang sangat sedih karena orang-orang musyrik Mekkah tidak mau beriman kepada ajarannya. Rasul berkeinginan agar semua orang menyambut ajakannya karena cintanya kepada manusia dan kasih sayangnya kepada mereka, sehingga seolah-olah beliau ingin membinasakan diri sendiri melihat penolakan mereka. Kata b\u0101khi\u2019 disebutkan dalam Al-Qur\u2019an dua kali, pada ayat ini dan pada Surah al-Kahf/18: 6.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}