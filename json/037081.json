{
    "id": 3869,
    "surah_id": 37,
    "ayah": 81,
    "page": 449,
    "quarter_hizb": 45.5,
    "juz": 23,
    "manzil": 6,
    "arabic": "\u0627\u0650\u0646\u0651\u064e\u0647\u0657 \u0645\u0650\u0646\u0652 \u0639\u0650\u0628\u064e\u0627\u062f\u0650\u0646\u064e\u0627 \u0627\u0644\u0652\u0645\u064f\u0624\u0652\u0645\u0650\u0646\u0650\u064a\u0652\u0646\u064e",
    "latin": "Innah\u016b min \u2018ib\u0101dinal-mu'min\u012bn(a).",
    "translation": "Sesungguhnya dia termasuk hamba-hamba Kami yang mukmin.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 37,
        "arabic": " \u0627\u0644\u0635\u0651\u0670\u06e4\u0641\u0651\u0670\u062a",
        "latin": "A\u1e63-\u1e62\u0101ff\u0101t",
        "transliteration": "As-Saffat",
        "translation": "Barisan-Barisan",
        "num_ayah": 182,
        "page": 446,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Nabi Nuh adalah orang yang berbuat baik karena sungguh, dia termasuk di antara hamba-hamba Kami yang beriman, jujur, dan ikhlas.",
        "tahlili": "Pengabadian nama Nuh dengan sebutan salam sejahtera kepadanya itu merupakan penghormatan kepadanya, dan pembalasan kepadanya atas kebajikan yang diperbuatnya dan perjuangannya dalam menegakkan kalimat tauhid yang tak henti-hentinya, siang dan malam, terang-terangan dan sembunyi-sembunyi selama ratusan tahun. Hal itu juga sebagai imbalan atas kesabarannya, dalam menahan derita lahir dan batin selama menyampaikan risalah di tengah-tengah kaumnya.\nYang mendorong Nabi Nuh bekerja keras membimbing kaumnya adalah kemurnian dan keikhlasan pengabdiannya kepada Allah disertai keteguhan iman dalam jiwanya. Oleh karena itu, Allah menyatakan bahwa dia benar-benar hamba-Nya yang penuh iman. Penonjolan iman pada pribadi Nuh sebagai rasul yang mendapat pujian adalah untuk menunjukkan arti yang besar terhadap iman itu karena dia merupakan modal dari segala amal perbuatan kebajikan.\nAdapun kaum Nuh yang lain, yang tidak mau beriman kepada agama tauhid yang disampaikan kepada mereka, dibinasakan oleh topan dan banjir besar hingga tak seorang pun di antara mereka yang tinggal dan tak ada pula bekas peninggalan mereka yang dikenang. Mereka lenyap dari catatan sejarah manusia.",
        "intro_surah": "Surah ini merupakan surah ke-37 dalam Al-Qur\u2019an dan termasuk dalam kelompok surah-surah Makkiyyah, diturunkan sesudah Surah al-An\u2018\u0101m. Ia terdiri dari 182 ayat.\nNama surah ini diambil dari kata-kata \u201ca\u1e63-\u1e62\u0101ff\u0101t\u201d yang terdapat pada ayat yang pertama, a\u1e63-\u1e62\u0101ff\u0101t berarti \u201cyang berbaris-baris\u201d.\nPada permulaan surah ini diterangkan keadaan malaikat yang berbaris-baris dengan jiwa yang bersih di hadapan Tuhan, tidak dapat digoda oleh iblis dan setan.\nIni semua dikemukakan untuk menjadi pelajaran dan kaca perbandingan tentang ketaatan dan penghambaan diri yang tulus ikhlas kepada Allah swt.\n\nPokok-pokok Isinya:\n1. \tKeimanan:\nPada bagian ini dibentangkan dalil-dalil tentang kemahaesaan Allah dan tentang adanya hari Kebangkitan, Padang Mahsyar di hari Kiamat, dan tentang malaikat yang selalu bertasbih kepada Allah.\n2. \tKisah-kisah:\nTentang Nabi Nuh, Ismail, Musa, Harun, Ilyas, Lut, dan Yunus.\n3. \tLain-lain:\nSikap orang-orang kafir terhadap Al-Qur\u2019an, saling tuduh-menuduh antara kaum kafir dan para pengikut mereka pada hari Kiamat; kenikmatan di surga yang diperoleh orang-orang yang beriman; kisah tentang pohon zaqqum; celaan terhadap orang-orang yang mengatakan bahwa Allah mempunyai anak; penjelasan bahwa yang baik belum tentu menurunkan keturunan yang baik pula. ",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH Y\u0100S\u012aN DENGAN\nSURAH A\u1e62-\u1e62\u0100FF\u0100T\n\n1. \tPada Surah Y\u0101s\u012bn disebut secara umum tentang umat-umat yang telah dihancurkan Allah karena ingkar kepada-Nya, sedang Surah a\u1e63-\u1e62\u0101ff\u0101t menjelaskan kisah-kisah itu dengan menyebut kisah-kisah Ibrahim, Isa, dengan kaumnya.\n2. \tPada akhir Surah Y\u0101s\u012bn disebut secara umum keadaan orang-orang mukmin dan orang kafir di hari Kiamat, sedang Surah a\u1e63-\u1e62\u0101ff\u0101t menjelaskannya.\n3. \tPada Surah Y\u0101s\u012bn disebutkan tentang kekuasaan Allah membangkitkan manusia dan menghidupkannya kembali, karena Dialah yang menciptakan mereka dan Dialah yang menghendakinya demikian, sedang Surah a\u1e63-\u1e62\u0101ff\u0101t menjelaskan lebih luas dengan mengemukakan contoh-contoh yang berhubungan dengan itu.\n",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu dijelaskan tentang kesesatan umat-umat terdahulu dan penolakan mereka terhadap seruan para rasul untuk beriman kepada Allah, hari kebangkitan dan penghitungan amal. Pada ayat-ayat berikut ini, Allah menjelaskan secara rinci tentang penyelamatan Nabi Nuh terhadap kaumnya yang beriman dan kebinasaan bagi yang ingkar terhadap seruannya.",
        "theme_group": "PENYELAMATAN NUH DAN PENGIKUTNYA",
        "kosakata": "Kosakata: Al-Muj\u012bb\u016bn \u0627\u0644\u0652\u0645\u064f\u062c\u0650\u064a\u0652\u0628\u064f\u0648\u0652\u0646\u064e (a\u1e63-\u1e62\u0101ff\u0101t/37: 75)\nSecara kebahasaan, al-muj\u012bb\u016bn yang merupakan bentuk jamak (plural) dari al-muj\u012bb bermakna yang menjawab atau yang memperkenankan. Dalam konteks ayat di atas, al-muj\u012bb\u016bn dialamatkan kepada Allah sebagai Zat yang mengabulkan seruan doa Nabi Nuh. Diceritakan, ketika Nabi Nuh merasa yakin bahwa kaumnya tidak ada harapan lagi untuk beriman kepada Allah, maka beliau berdoa kepada Allah supaya menurunkan azab kepada mereka. Menjawab seruan doa Nabi Nuh itu, Allah swt menyatakan, \u201cSesungguhnya Nuh telah menyeru kepada Kami; maka sesungguhnya sebaik-baik yang memperkenankan (adalah Kami).\u201d Allah pun lalu menurunkan azab kepada mereka, kecuali pengikut setia Nabi Nuh. Allah menegaskan, \u201cDan Kami telah menyelamatkannya dan pengikutnya dari bencana yang besar.\u201d\n",
        "sabab_nuzul": null,
        "conclusion": null
    }
}