{
    "id": 5728,
    "surah_id": 79,
    "ayah": 16,
    "page": 584,
    "quarter_hizb": 59,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0627\u0650\u0630\u0652 \u0646\u064e\u0627\u062f\u0670\u0649\u0647\u064f \u0631\u064e\u0628\u0651\u064f\u0647\u0657 \u0628\u0650\u0627\u0644\u0652\u0648\u064e\u0627\u062f\u0650 \u0627\u0644\u0652\u0645\u064f\u0642\u064e\u062f\u0651\u064e\u0633\u0650 \u0637\u064f\u0648\u064b\u0649\u06da",
    "latin": "I\u017c n\u0101d\u0101hu rabbuh\u016b bil-w\u0101dil-muqaddasi \u1e6duw\u0101(n).",
    "translation": "(Ingatlah) ketika Tuhannya menyeru dia (Musa) di lembah suci, yaitu Lembah Tuwa,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 79,
        "arabic": " \u0627\u0644\u0646\u0651\u0670\u0632\u0639\u0670\u062a",
        "latin": "An-N\u0101zi\u2018\u0101t",
        "transliteration": "An-Nazi\u2018at",
        "translation": "Yang Mencabut Dengan Keras",
        "num_ayah": 46,
        "page": 583,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Ingatlah kisah ketika Tuhan memanggilnya di lembah suci, yaitu Lembah Tuwa; ",
        "tahlili": "Dalam ayat ini, Allah mengingatkan Nabi Muhammad tentang kisah Musa dalam bentuk pertanyaan, yaitu apakah belum diketahui olehnya tentang kisah Musa yang diutus Allah kepada Fir\u2018aun untuk menyampaikan risalahnya dengan cara yang halus dan lemah lembut seperti tercantum dalam firman Allah:\n\u0641\u064e\u0642\u064f\u0648\u0652\u0644\u064e\u0627 \u0644\u064e\u0647\u0657 \u0642\u064e\u0648\u0652\u0644\u064b\u0627 \u0644\u0651\u064e\u064a\u0651\u0650\u0646\u064b\u0627 \u0644\u0651\u064e\u0639\u064e\u0644\u0651\u064e\u0647\u0657 \u064a\u064e\u062a\u064e\u0630\u064e\u0643\u0651\u064e\u0631\u064f \u0627\u064e\u0648\u0652 \u064a\u064e\u062e\u0652\u0634\u0670\u0649 \u0664\u0664\nMaka berbicaralah kamu berdua kepadanya (Fir\u2018aun) dengan kata-kata yang lemah lembut, mudah-mudahan dia sadar atau takut. (\u1e6c\u0101h\u0101/20: 44)\n\nKisah Nabi Musa terutama tatkala Tuhan memanggil Musa di lembah suci yaitu di Lembah \u1e6cuw\u0101 di dekat Gunung Sinai. Pada saat itu, Nabi Musa bermunajat kepada Allah sebagaimana dijelaskan dalam ayat berikut ini.",
        "intro_surah": "Surah an-N\u0101zi\u2018\u0101t terdiri dari 46 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah an-Naba\u2019.\nNama an-N\u0101zi\u2018\u0101t (Malaikat-malaikat yang mencabut) diambil dari kata an-N\u0101zi\u2018\u0101t yang terdapat pada ayat pertama surah ini.\nSurah ini dinamai pula dengan as-S\u0101hirah yang diambil dari ayat 14, dan dinamai juga a\u1e6d-\u1e6c\u0101mmah yang diambil dari ayat 34.\n\nPokok-pokok Isinya:\n1.\tKeimanan:\nPenegasan Allah tentang adanya hari Kiamat dan sikap orang-orang musyrik terhadapnya; manusia dibagi dua golongan di akhirat; manusia tidak dapat mengetahui kapan terjadinya saat Kiamat.\n2.\tKisah:\nKisah Nabi Musa dengan Fir\u2018aun.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AN-NABA\u2019\nDENGAN SURAH AN-N\u0100ZI\u2018\u0100T\n\n1. \tSurah an-Naba\u2019 menerangkan ancaman Allah terhadap sikap orang-orang musyrik yang mengingkari adanya hari kebangkitan, serta mengemukakan bukti-bukti adanya hari kebangkitan, sedangkan pada Surah an-N\u0101zi\u2018\u0101t, Allah bersumpah bahwa hari Kiamat yang mendahului hari kebangkitan itu pasti terjadi.\n2. \tSama-sama menerangkan huru-hara yang terjadi pada hari Kiamat dan hari kebangkitan.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menerangkan sikap orang-orang kafir Mekah yang terus-menerus mengingkari hari kebangkitan dan mengejek Rasulullah saw dengan ejekan yang sangat menyakitkan hati. Pada ayat-ayat berikut ini, Allah membentangkan kisah Musa dengan Fir\u2018aun. Allah menjelaskan bahwa Fir\u2018aun lebih jahat dan lebih kufur daripada kaum Quraisy di Mekah. Ia telah mengangkat dirinya sebagai tuhan yang harus disembah, dan telah membujuk kaumnya supaya menentang seruan Nabi Musa.",
        "theme_group": "KISAH MUSA DAN FIR\u2018AUN SEBAGAI\nHIBURAN BAGI NABI MUHAMMAD",
        "kosakata": "Kosakata: Nak\u0101l \u0646\u064e\u0643\u064e\u0627\u0644\u064e (an-N\u0101zi\u2018\u0101t/79: 25)\nNak\u0101l artinya siksaan. Terambil dari kata kerja nakala yang artinya \u2018lemah\u2019. Nak\u0101l berarti \u201csiksaan\u201d karena siksaan itu membuat orang lemah tak berdaya. Dalam Al-Qur\u2019an juga terdapat kata ank\u0101l, bentuk jamak dari nakl, yang secara harfiah berarti \u201ckekang\u201d atau \u201ctambatan\u201d hewan, karena keduanya membuat hewan itu harus patuh dan tentu saja tersiksa. Dalam Surah al-Muzzammil/73: 12 disebutkan bahwa Allah memiliki alat-alat untuk menyiksa dan neraka bagi orang kafir: inna ladain\u0101 ank\u0101lan wa ja\u1e25\u012bman (sungguh, di sisi Kami ada belenggu-belenggu (yang berat) dan neraka yang menyala-nyala). Juga terdapat kata tank\u012bl yang merupakan ma\u1e63dar dari nakkala (menyiksa).",
        "sabab_nuzul": null,
        "conclusion": null
    }
}