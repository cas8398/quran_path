{
    "id": 6054,
    "surah_id": 91,
    "ayah": 11,
    "page": 595,
    "quarter_hizb": 60.25,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0643\u064e\u0630\u0651\u064e\u0628\u064e\u062a\u0652 \u062b\u064e\u0645\u064f\u0648\u0652\u062f\u064f \u0628\u0650\u0637\u064e\u063a\u0652\u0648\u0670\u0649\u0647\u064e\u0627\u0653  \u06d6",
    "latin": "Ka\u017c\u017cabat \u1e61am\u016bdu bi\u1e6dagw\u0101h\u0101.",
    "translation": "(Kaum) Samud telah mendustakan (rasulnya) karena mereka melampaui batas ",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 91,
        "arabic": "\u0627\u0644\u0634\u0651\u0645\u0633",
        "latin": "Asy-Syams",
        "transliteration": "Asy-Syams",
        "translation": "Matahari",
        "num_ayah": 15,
        "page": 595,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Kaum Samud, yang dahulu tinggal di sebelah selatan Madinah, adalah contoh manusia yang mengotori jiwa dengan kekafiran dan maksiat. Kaum Samud telah mendustakan rasulnya, yaitu Nabi Saleh, karena mereka melampaui batas dalam keingkaran terhadap ajakan nabi mereka dan melakukan tindakan yang penuh dosa.",
        "tahlili": "Kaum Samud adalah umat Nabi Saleh. Mereka telah mendustakan dan mengingkari kenabian dan ajaran-ajaran yang dibawa Nabi Saleh dari Allah. Nabi Saleh diberi mukjizat oleh Allah sebagai ujian bagi kaumnya, yaitu seekor unta betina yang dijelmakan dari sebuah batu besar, untuk menandingi keahlian kaum itu yang sangat piawai dalam seni patung dari batu. Bila mereka piawai dalam seni patung sehingga patung itu terlihat bagaikan hidup, maka mukjizat Nabi Saleh adalah menjelmakan seekor unta betina yang benar-benar hidup dari sebuah batu. Akan tetapi, mereka tidak mengakuinya, dan berusaha membunuh unta itu.",
        "intro_surah": "Surah asy-Syams terdiri dari 15 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-Qadar dan sebelum Surah al-Bur\u016bj.\nNama asy-Syams diambil dari kata itu yang disebutkan pada ayat pertama.\n\nPokok-pokok Isinya:\n1.\tSumpah Allah dengan matahari dan bulan, siang dan malam, langit dan bumi untuk menekankan bahwa manusia memiliki dua sisi yang bertolak belakang dalam dirinya. Oleh karena itu, ia harus mengembangkan sisi yang positif dari dirinya.\n2.\tContoh seorang yang mengikuti segi negatif dalam dirinya adalah seorang yang diinformasikan bernama Qudar bin Salif dari kaum Samud. Ia menjadi seorang paling jahat dari kaumnya. Ia membunuh unta betina yang merupakan mukjizat Nabi Saleh, sehingga akibatnya ia dan kaumnya ditimpa azab dari Allah.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-BALAD\nDENGAN SURAH ASY-SYAMS\n\n1.\tKedua surah menerangkan adanya dua golongan manusia, yaitu yang kanan dan yang kiri, yang berbuat baik dan yang tidak berbuat baik, yang berjasa dan yang tidak berbuat apa-apa, dan yang masuk surga dan yang masuk neraka.\n2.\tKedua surah juga menyampaikan pesan bahwa manusia perlu berusaha dan berjuang untuk memperoleh kebahagiaan dengan menyelesaikan pekerjaan-pekerjaan besar dan sulit.",
        "munasabah_prev_theme": "Dalam ayat-ayat yang lalu dinyatakan bahwa orang yang mengembangkan potensi baiknya akan bahagia, dan orang yang mengikuti potensi jahatnya akan celaka. Dalam ayat-ayat berikut diberikan contoh orang-orang yang mengikuti potensi jahatnya itu yaitu kaum Samud.",
        "theme_group": "CONTOH MANUSIA CELAKA ADALAH KAUM SAMUD",
        "kosakata": "Kosakata: Fadamdama \u0641\u064e\u062f\u064e\u0645\u0652\u062f\u064e\u0645\u064e (asy-Syams/91: 14)\nMakna damdama adalah menghancurkan secara menyeluruh dan membuat panik. Ad-damdamah berarti cerita suara kucing. Damama juga diartikan dengan mencelupkan sesuatu sehingga menjadi rata. Damamtu\u1e61-\u1e61aub berarti aku mencelupkan baju dengan zat pewarna sehingga warnanya menjadi rata. Sementara ulama berpendapat bahwa damdama berarti mengguncang bangunan sehingga menjadi rata dengan tanah. Ada juga yang memahaminya dalam arti meratakan dengan tanah. Selain itu, ad-damdamah juga mengandung makna kemarahan dan kebencian. Yang jelas, makna damdama menggambarkan tentang siksa dari Allah yang marah melihat kemaksiatan yang dilakukan makhluk-Nya. Siksa ini bersifat menyeluruh dan merata, serta menimpa semua yang terlibat. Kata damdama hanya terulang sekali dalam Al-Qur\u2019an yaitu dalam ayat ini. \nAyat ini menjelaskan tentang siksa Allah kepada kaum Nabi Saleh yaitu kaum Samud. Mereka mendustakan dan bahkan menantang kebenaran risalah yang dibawanya. Allah memberikan mukjizat kepada Nabi Saleh berupa unta betina. Nabi Saleh memperingatkan kaumnya agar tidak mengganggu unta betina itu atau menghalanginya untuk minum pada hari yang telah dikhususkan. Akan tetapi, kaum Samud malah menyembelihnya. Karena kedurhakaan yang mereka lakukan, kemudian Allah menurunkan azab berupa suara yang menggelegar yang menjungkirbalikkan tanah tempat mereka berpijak. Azab ini bersifat menyeluruh untuk seluruh kaum Samud walaupun yang melakukan penyembelihan hanya sebagian saja. Tetapi karena sikap diam sebagian yang lain dan membiarkan sebagian yang lain berbuat durhaka, maka Allah menyamaratakan siksaan tersebut.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}