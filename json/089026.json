{
    "id": 6019,
    "surah_id": 89,
    "ayah": 26,
    "page": 594,
    "quarter_hizb": 60,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0648\u0651\u064e\u0644\u064e\u0627 \u064a\u064f\u0648\u0652\u062b\u0650\u0642\u064f \u0648\u064e\u062b\u064e\u0627\u0642\u064e\u0647\u0657\u0653 \u0627\u064e\u062d\u064e\u062f\u064c \u06d7",
    "latin": "Wa l\u0101 y\u016b\u1e61iqu wa\u1e61\u0101qah\u016b a\u1e25ad(un).",
    "translation": "Tidak ada seorang pun juga yang mampu mengikat (sekuat) ikatan-Nya.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 89,
        "arabic": " \u0627\u0644\u0641\u062c\u0631",
        "latin": "Al-Fajr",
        "transliteration": "Al-Fajr",
        "translation": "Fajar",
        "num_ayah": 30,
        "page": 593,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Dan tidak ada seorang pun yang mengikat seperti ikatan-Nya. Ikatan Allah sangat kukuh dan kuat. Tidak ada kekuatan yang mampu mengendurkan ikatan itu, apalagi melepaskannya.",
        "tahlili": "Pada waktu itu tidak ada yang lebih dipercaya dalam melaksanakan tugasnya selain Malaikat Zabaniyah. Malaikat itu akan melaksanakan tugasnya persis sebagaimana yang diperintahkan Allah, yaitu bahwa orang-orang yang durhaka itu akan diazab di dalam neraka Jahanam sesuai dengan dosa-dosa mereka. Dengan demikian, terbuktilah bahwa kelimpahan nikmat yang mereka terima pada waktu di dunia itu bukanlah tanda bahwa Allah cinta kepada mereka.",
        "intro_surah": "Surah ini terdiri dari 30 ayat, termasuk kelompok surah Makkiyyah, turun sebelum Surah a\u1e0d-\u1e0cuh\u0101 dan sesudah Surah al-F\u012bl. Nama al-Fajr diambil dari kata al-fajr yang terdapat pada ayat pertama surah ini yang artinya \u201cfajar\u201d.\n\nPokok-pokok Isinya:\nAllah bersumpah dengan fajar dan malam setiap hari atau hari-hari tertentu untuk menekankan bahwa apa dan siapa pun di alam ini tidak akan abadi. Contohnya adalah beberapa umat terdahulu yang dihancurkan Allah karena kedurhakaan mereka walaupun mereka begitu kuat dan perkasa. Peristiwa-peristiwa itu hendaknya menjadi peringatan bagi kaum kafir Mekah bahwa bila mereka tetap membangkang, mereka dapat saja dihancurkan oleh Allah seperti umat-umat itu. Manusia secara pribadi juga demikian, mereka akan mati, kemudian akan menjalani kehidupan yang abadi di akhirat dalam keadaan bahagia atau sengsara. Oleh karena itu, manusia jangan terlalu cinta harta, sebab kecukupan materi di dunia belum tentu merupakan pertanda bahwa Allah mencintainya. Yang diperlukan adalah mencintai anak yatim, membantu orang miskin, dan tidak memakan harta pusaka yang tidak menjadi haknya. Orang yang bersih dari dosa akan dipersilakan oleh Allah untuk memasuki surga-Nya.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-G\u0100SYIYAH\nDENGAN SURAH AL-FAJR\n\n1. \tDalam Surah al-G\u0101syiyah, Allah menyebutkan tentang orang yang pada hari Kiamat tergambar di mukanya kehinaan, dan tentang orang yang bercahaya wajahnya. Sedang pada Surah al-Fajr disebutkan beberapa kaum yang mendustakan dan berbuat durhaka, sebagai contoh dari orang-orang yang tergambar di muka mereka kehinaan dan azab yang ditimpakan kepada mereka di dunia. Disebutkan pula orang yang berjiwa mu\u1e6dmainnah, mereka itulah orang-orang yang wajahnya bercahaya.\n2. \tDalam Surah al-G\u0101syiyah, Allah mengemukakan orang yang bercahaya wajahnya, sedang pada surah al-Fajr disebutkan orang yang berjiwa tenang di dunia karena iman dan takwanya yang di akhirat akan berseri-seri wajahnya.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu diterangkan bahwa orang yang kafir, walaupun berkecukupan, mereka tidak menaruh perhatian pada anak yatim dan orang miskin. Kerja mereka hanya mengumpulkan harta tanpa peduli pada kehalalan dan keharamannya. Pada ayat-ayat berikut diterangkan bahwa mereka akan menyesal nanti di akhirat.",
        "theme_group": "PENYESALAN ORANG KAFIR PADA HARI KIAMAT",
        "kosakata": "Kosakata: Dakkan \u062f\u064e\u0643\u0651\u064b\u0627 (al-Fajr/89: 21)\nKata dakkan berarti guncangan, ma\u1e63dar (kata jadian) dari kata dakka yang berarti menghancurkan gunung, dinding, dan semisalnya. Di dalam Al-Qur\u2019an, kata ini diulang sebanyak tujuh kali dalam beberapa bentuk. Di antaranya adalah dakk\u0101\u2019 yang berarti hancur luluh, yaitu di dalam firman Allah, \u201cDia (Zulkarnain) berkata, \u2018(Dinding) ini adalah rahmat dari Tuhanku, maka apabila janji Tuhanku sudah datang, Dia akan menghancurluluhkannya; dan janji Tuhanku itu benar\u2019.\u201d (al-Kahf/18: 98)",
        "sabab_nuzul": null,
        "conclusion": "1.\tDunia ini akan hancur, dan setelah itu dihidupkan kembali, lalu mulailah kehidupan akhirat.\n2.\tDi akhirat manusia akan diperiksa segala perbuatannya. Orang yang durhaka akan dihadapkan ke neraka Jahanam. Waktu itulah mereka sadar lalu menyesali kekeliruan pandangan mereka dulu pada waktu di dunia bahwa mereka disayangi oleh Allah.\n3.\tSadar sebelum terjadi itulah yang baik; iman sebelum menutup mata itulah yang menyelamatkan."
    }
}