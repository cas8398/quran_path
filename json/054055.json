{
    "id": 4901,
    "surah_id": 54,
    "ayah": 55,
    "page": 531,
    "quarter_hizb": 53.75,
    "juz": 27,
    "manzil": 7,
    "arabic": "\u0641\u0650\u064a\u0652 \u0645\u064e\u0642\u0652\u0639\u064e\u062f\u0650 \u0635\u0650\u062f\u0652\u0642\u064d \u0639\u0650\u0646\u0652\u062f\u064e \u0645\u064e\u0644\u0650\u064a\u0652\u0643\u064d \u0645\u0651\u064f\u0642\u0652\u062a\u064e\u062f\u0650\u0631\u064d \u08d6 ",
    "latin": "F\u012b maq\u2018adi \u1e63idqin \u2018inda mal\u012bkim muqtadir(in).",
    "translation": " di tempat yang disenangi di sisi Tuhan Yang Maha Kuasa.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 54,
        "arabic": " \u0627\u0644\u0642\u0645\u0631",
        "latin": "Al-Qamar",
        "transliteration": "Al-Qamar",
        "translation": "Bulan",
        "num_ayah": 55,
        "page": 528,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Mereka tinggal di tempat yang disenangi dan penuh kebahagiaan, di sisi Tuhan Yang Mahakuasa.",
        "tahlili": "Bagi mereka yang bertakwa, Allah memberikan surga-surga sesuai tingkat ketakwaan mereka. Sebagaimana diketahui surga itu bertingkat-tingkat. Di dalam surga-surga mengalir sungai-sungai yang menunjukkan bahwa surga adalah tempat yang menyejukkan, indah dan memberikan hasil yang banyak. Mereka menempati tempat yang benar yang tidak ada cacat atau kekurangannya dan mereka berada di bawah naungan Maharaja yang Mahakuasa, yang akan memberi mereka apa yang Ia kehendaki tanpa halangan siapa pun.",
        "intro_surah": "Surah al-Qamar terdiri dari 55 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah a\u1e6d-\u1e6c\u0101riq.\nNama al-Qamar (bulan) diambil dari kata al-Qamar (yang terdapat pada ayat yang pertama surah ini). Pada ayat ini diterangkan tentang terbelahnya bulan sebagai mukjizat.\n\nPokok-pokok Isinya:\n1. \tKeimanan:\nPemberitaan bahwa datangnya hari Kiamat sudah dekat, semua yang ada pada alam adalah dengan ketetapan Allah; kehendak Allah pasti berlaku, tiap-tiap pekerjaan manusia dicatat oleh malaikat.\n2. \tKisah-kisah:\nKisah kaum yang mendustakan rasul-rasul di masa dahulu seperti kaum Nuh, \u2018\u0100d, \u1e60am\u016bd dan Fir\u2018aun.\n3. \tLain-lain:\nOrang-orang kafir dikumpulkan di akhirat dalam keadaan hina dan akan menerima balasan yang setimpal; celaan terhadap orang-orang yang tidak memperhatikan ayat-ayat Al-Qur\u2019an.",
        "outro_surah": "Surah al-Qamar berisi peringatan Allah tentang adanya hari Kiamat, kisah tentang umat-umat terdahulu yang mendustakan rasul-rasul mereka agar menjadi pelajaran bagi umat-umat yang datang kemudian, dan peringatan terhadap orang kafir bahwa mereka akan diazab pada hari Kiamat serta balasan yang menyenangkan akan diterima oleh orang-orang yang takwa.",
        "munasabah_prev_surah": "1.\tPada akhir Surah an-Najm disebutkan hal yang mengenai hari Kiamat sedang pada awal Surah al-Qamar disebutkan pula hal itu.\n2.\tDalam Surah an-Najm disinggung secara sepintas keadaan umat-umat yang terdahulu, sedang pada Surah al-Qamar juga disebutkan keadaan umat-umat yang terdahulu, yang mendustakan rasul-rasul mereka.",
        "munasabah_prev_theme": "Dalam ayat yang lalu Allah swt mengancam kaum kafir bahwa mereka akan dihancurkan di dunia dan akan diazab dalam neraka di akhirat. Dalam ayat-ayat berikutnya Allah menerangkan bagaimana azab yang akan diterima orang-orang yang berdosa, dan kebahagiaan yang akan diterima orang-orang yang takwa di akhirat. ",
        "theme_group": "SIKSA TERHADAP YANG BERDOSA\nDAN PAHALA BAGI YANG BERTAKWA  ",
        "kosakata": "Kosakata:\n1.\tSaqar \u0633\u064e\u0642\u064e\u0631\u064e (al-Qamar/54: 48).\nKata saqar biasa diartikan sebagai nama bagi salah satu neraka, yaitu neraka saqar. Kata saqar disebutkan empat kali dalam Al-Qur\u2019an, tiga di antaranya disebutkan dalam Surah al-Mudda\u1e61\u1e61ir/74: 26, 27 dan 42, dan satu kali disebutkan dalam Surah al-Qamar/54: 48.\nKeempat kata saqar yang terdapat dalam Al-Qur\u2019an disebutkan dalam konteks siksaan di akhirat. Karena itu kata tersebut diartikan sebagai salah satu nama tempat penyiksaan di hari akhirat, atau nama bagi salah satu tingkat tempat penyiksaan tersebut. Menurut al-Qur\u1e6duby, saqar adalah tingkat keenam dari tujuh tingkat neraka.\nDari segi bahasa, kata saqar berasal dari kata kerja saqara, yang pada mulanya menurut Ibnu Faris, digunakan untuk mengungkapkan sesuatu yang menyengat, atau memberi tanda pada binatang dengan cara membakar kulitnya dengan besi panas, dan mengubah warna sesuatu yang terbakar. Sejalan dengan perkembangan peradaban umat manusia, arti saqar berkembang menjadi beraneka ragam seperti yang ditemui di dalam kamus-kamus bahasa Arab, misalnya, terik matahari dinamakan saqar, karena terik panas matahari dapat menyengat tubuh sehingga warnanya berubah. Besi panas yang digunakan untuk menandai binatang, dinamakan saqur. Orang kafir yang memusuhi umat Islam, disebut as-saqqar, karena hati mereka panas terus (sakit terus). Api neraka dinamai saqar, karena panasnya dan menghanguskan semua yang masuk ke dalamnya.\n2.\tMusta\u1e6dar \u0645\u064f\u0633\u0652\u062a\u064e\u0637\u064e\u0631 (al-Qamar/54: 53).\nKata musta\u1e6dar berasal dari fi\u2018il sa\u1e6dara-yas\u1e6duru-sa\u1e6dran berarti menulis, atau pengaturan huruf-huruf yang ditulis dengan rapi dan indah. Jadi kata musta\u1e6dar berarti tertulis dengan rapi dan teliti. Ketelitian dan kerapiannya itu diperkuat lagi dengan penambahan huruf t\u0101' yang mendahului huruf \u1e6d\u0101'.\nKata musta\u1e6dar hanya satu kali disebutkan dalam Al-Qur\u2019an yaitu pada Surah al-Qamar/54: 53 tersebut, tetapi kata yang seakar dengannya disebutkan 4 kali dalam Al-Qur\u2019an yaitu yas\u1e6dur\u016bn pada Surah al-Qalam/68:1, yang berarti menulis, dan mas\u1e6d\u016br 3 kali yaitu pada Surah a\u1e6d-\u1e6c\u016br/52:2, al-Isr\u0101'/17: 58 dan al-A\u1e25z\u0101b/33: 6, semuanya berarti tertulis.",
        "sabab_nuzul": null,
        "conclusion": "1.\tOrang jahat akan dimasukkan Allah ke dalam neraka, karena kekafiran dan perbuatan jahat mereka yang tercatat secara akurat di dalam buku amal mereka masing-masing.\n2.\tOrang-orang yang bertakwa akan dimasukkan ke dalam surga yang penuh nikmat sesuai dengan iman dan perbuatan baik masing-masing dan berada di bawah naungan Allah swt.\n3.\tManusia wajib berikhtiar, hasilnya ditentukan oleh Allah swt."
    }
}