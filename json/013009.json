{
    "id": 1716,
    "surah_id": 13,
    "ayah": 9,
    "page": 250,
    "quarter_hizb": 25.75,
    "juz": 13,
    "manzil": 3,
    "arabic": "\u0639\u0670\u0644\u0650\u0645\u064f \u0627\u0644\u0652\u063a\u064e\u064a\u0652\u0628\u0650 \u0648\u064e\u0627\u0644\u0634\u0651\u064e\u0647\u064e\u0627\u062f\u064e\u0629\u0650 \u0627\u0644\u0652\u0643\u064e\u0628\u0650\u064a\u0652\u0631\u064f \u0627\u0644\u0652\u0645\u064f\u062a\u064e\u0639\u064e\u0627\u0644\u0650 ",
    "latin": "\u2018Alimul-gaibi wasy-syah\u0101datil-kab\u012brul-muta\u2018\u0101l(i).",
    "translation": "(Allahlah) yang mengetahui semua yang gaib dan yang nyata. (Dia) Yang Maha Besar lagi Maha Tinggi.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 13,
        "arabic": " \u0627\u0644\u0631\u0651\u0639\u062f",
        "latin": "Ar-Ra\u2018d",
        "transliteration": "Ar-Ra\u2018d",
        "translation": "Guruh",
        "num_ayah": 43,
        "page": 249,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Allah adalah Tuhan Yang Maha Mengetahui semua yang gaib dan yang nyata; Dia adalah Tuhan Yang Mahabesar, Mahatinggi.",
        "tahlili": "Ayat ini menjelaskan bahwa Dialah Tuhan Yang Mengetahui yang gaib, yang tampak, dan yang tidak bisa diketahui oleh pancaindra manusia. Ilmu pengetahuan telah membuktikan bahwa ada makhluk yang tidak dapat dilihat dengan mata telanjang, karena kecil sekali. Ia baru dapat dilihat dengan mikroskop dan teleskop, seperti bakteri dan virus yang dapat menularkan bermacam-macam penyakit yang sulit sekali untuk diberantas, atau sampai sekarang belum ditemukan obat pembasminya. Bakteri dan virus itu termasuk tentara Allah, yang tidak dapat diketahui berapa jumlahnya melainkan oleh Allah sendiri, seperti diterangkan dalam firman-Nya:\n\u0648\u064e\u0645\u064e\u0627 \u064a\u064e\u0639\u0652\u0644\u064e\u0645\u064f \u062c\u064f\u0646\u064f\u0648\u0652\u062f\u064e \u0631\u064e\u0628\u0651\u0650\u0643\u064e \u0627\u0650\u0644\u0651\u064e\u0627 \u0647\u064f\u0648\u064e\u06d7 \nDan tidak ada yang mengetahui bala tentara Tuhanmu kecuali Dia sendiri. (al-Mudda\u1e61\u1e61ir/74: 31)",
        "intro_surah": "Surah ar-Ra\u2019d ini terdiri atas 43 ayat termasuk golongan surah Makkiyah menurut Ibnu \u2018Abb\u0101s, Hasan, Ikrimah, dan lain-lain. Sedangkan Ibnu \u2018Abb\u0101s, Zubair, Muqatil, dan lain-lain menggolongkannya ke dalam kelompok surah Madaniyah.\nSurah ini dinamakan ar-Ra\u2018d yang berarti \u201cguruh\u201d atau \u201cguntur\u201d karena dalam ayat 13 Allah berfirman yang artinya \u201cDan guruh itu bertasbih sambil memuji-Nya\u201d, menunjukkan sifat kesucian dan kesempurnaan Allah swt. Juga sesuai dengan sifat Al-Qur\u2019an yang mengandung ancaman dan harapan, maka demikian pulalah halnya bunyi guruh itu menimbulkan kecemasan dan harapan bagi manusia. Bagian terpenting dari isi surah ini ialah bahwa bimbingan Allah kepada makhluk-Nya bertalian erat dengan hukum sebab dan akibat. Bagi Allah swt tidak ada pilih kasih dalam menetapkan hukuman. Balasan atau hukuman adalah akibat dari ketaatan atau keingkaran terhadap hukum Allah.\n\nPokok-pokok Isi:\n1.  Keimanan:\nAllah yang menciptakan alam semesta dan mengaturnya; ilmu Allah meliputi segala sesuatu; adanya malaikat yang selalu memelihara manusia yang datang silih berganti, hanya Allah yang mengabulkan doa dari hamba-Nya; memberi taufik dan kesuksesan hanya hak Allah, sedang tugas para rasul menyampaikan agama Allah.\n2.  Hukum:\nManusia dilarang mendoakan yang jelek untuk dirinya; kewajiban mencegah perbuatan-perbuatan yang mungkar.\n3.  Kisah:\nKisah perjalanan dakwah para rasul dan nabi.\n4.  Lain-lain:\nBeberapa sifat yang terpuji; perumpamaan bagi orang-orang yang menyembah berhala dan orang-orang yang menyembah Allah; Allah tidak mengubah nasib suatu bangsa sehingga mereka mengubah keadaan mereka sendiri. \n",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tDalam Surah Y\u016bsuf, Allah secara umum mengemukakan tanda-tanda keesaan Allah di langit dan bumi, dan dalam Surah ar-Ra\u2018d, Allah mengemukakannya secara lebih jelas.\n2. \tKedua surah tersebut masing-masing memuat perjalanan dakwah nabi-nabi zaman dahulu beserta umatnya. Kaum yang menentang kebenaran mengalami kehancuran, sedang yang mengikuti kebenaran mendapat kemenangan.\n3. \tPada akhir Surah Y\u016bsuf diterangkan bahwa Al-Qur\u2019an itu bukanlah perkataan yang diada-adakan, melainkan petunjuk dan rahmat bagi orang yang beriman, dan keterangan ini ditegaskan kembali di awal Surah ar-Ra\u2018d.\n",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menerangkan keingkaran orang-orang musyrikin tentang kebangkitan manusia pada hari kiamat setelah tubuh mereka hancur menjadi tanah untuk diadili amal perbuatannya. Pada ayat-ayat berikut, Allah swt menghilangkan keragu-raguan dan kemustahilan untuk menghidupkan kembali semua yang telah bercerai berai, karena Dia Maha Mengetahui yang nyata dan gaib, semua \u017carrah yang tersebar di langit maupun di bumi, dan apa yang ada di kandungan setiap perempuan.",
        "theme_group": "ALLAH MENGETAHUI SEGALA SESUATU",
        "kosakata": "Kosakata: Tag\u012b\u1e0du \u062a\u064e\u063a\u0650\u064a\u0652\u0636\u064f (ar-Ra\u2018d/13: 8)\nSecara kebahasaan, tag\u012b\u1e0du berarti kurang sempurna. Ayat di atas sedang menjelaskan bahwa Allah swt mengetahui apa saja yang dikandung oleh setiap perempuan, baik kandungan itu tag\u012b\u1e0du (kurang sempurna) maupun telah sempurna. Ini sekaligus menjelaskan ketidakterhinggaan pengetahuan Allah swt, bahwa barang apapun dan di manapun tidak akan luput dari pengetahuan-Nya.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}