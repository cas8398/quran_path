{
    "id": 5529,
    "surah_id": 74,
    "ayah": 34,
    "page": 576,
    "quarter_hizb": 58.25,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u0648\u064e\u0627\u0644\u0635\u0651\u064f\u0628\u0652\u062d\u0650 \u0627\u0650\u0630\u064e\u0627\u0653 \u0627\u064e\u0633\u0652\u0641\u064e\u0631\u064e\u06d9",
    "latin": "Wa\u1e63-\u1e63ub\u1e25i i\u017c\u0101 asfar(a).",
    "translation": "dan demi subuh apabila mulai terang,",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 74,
        "arabic": " \u0627\u0644\u0645\u062f\u0651\u062b\u0651\u0631",
        "latin": "Al-Mudda\u1e61\u1e61ir",
        "transliteration": "Al-Muddassir",
        "translation": "Orang Berselimut",
        "num_ayah": 56,
        "page": 575,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "dan demi subuh apabila mulai terang,",
        "tahlili": "Dalam ayat-ayat ini, Allah memperingatkan bahwa tidak ada jalan bagi manusia untuk mengingkari kekuasaan-Nya yang nyata-nyata dapat mereka saksikan sendiri.\nKata-kata \u201ckall\u0101\u201d (sekali-kali tidak) juga merupakan bantahan terhadap ucapan-ucapan orang musyrik di atas. Untuk menguatkan hal itu, Allah bersumpah dengan bulan, malam bila ia telah berlalu, dan bila subuh mulai bersinar. Dengan bulan, malam, dan subuh itu Allah menegaskan bahwa neraka Saqar itu merupakan suatu bencana yang amat dahsyat bagi umat manusia.\nAda yang menerangkan bahwa maksud i\u1e25dal-kubar (salah satu bencana yang sangat besar) adalah salah satu dari tujuh neraka yang dahsyat. Ketujuh lembah neraka (seperti yang disebutkan dalam ayat-ayat lain) itu adalah: Jahanam, La\u1e93a, Hu\u1e6damah, Sa\u2018\u012br, Saqar, Jah\u012bm, dan H\u0101wiyah.\nHal tersebut adalah sebagai ancaman bagi manusia. Adanya tujuh neraka itu (satu di antaranya Saqar) merupakan ancaman bagi yang masih tidak mau tunduk kepada kehendak Allah.\nAda yang mengartikan na\u017c\u012br (yang memberi ancaman) itu adalah sifat Allah, sehingga arti ayat ini adalah: \u201cAku ini memberikan ancaman kepadamu, karena itu hendaklah kamu takut kepada ancaman itu\u201d. Ada yang mengartikan na\u017c\u012br sebagai sifat Nabi Muhammad seperti disebutkan dalam ayat kedua di atas tadi.",
        "intro_surah": "Surah al-Mudda\u1e61\u1e61ir terdiri 56 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-Muzzammil. \nNama al-Mudda\u1e61\u1e61ir diambil dari perkataan al-mudda\u1e61\u1e61ir yang terdapat pada ayat pertama surah ini.\n\nPokok-pokok Isinya:\nPerintah untuk mulai berdakwah mengagungkan Allah, membersihkan pakaian, menjauhi maksiat, memberikan sesuatu dengan ikhlas, dan bersabar dalam menjalankan perintah serta menjauhi larangan Allah; Allah akan mengazab orang-orang yang menentang Nabi Muhammad dan mendustakan Al-Qur\u2019an; tiap-tiap manusia terikat dengan apa yang telah diusahakannya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tKedua surah ini sama-sama dimulai dengan seruan kepada Nabi Muhammad.\n2.\tSurah al-Muzzammil berisi perintah bangun di malam hari untuk melakukan salat Tahajud dan membaca Al-Qur\u2019an untuk menguatkan jiwa seseorang, sedangkan Surah al-Mudda\u1e61\u1e61ir berisi perintah melakukan dakwah menyucikan diri dan bersabar.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah telah menjelaskan tentang kehebatan neraka Saqar sebagai balasan bagi orang yang menuduh bahwa Al-Qur\u2019an itu ciptaan manusia, yang dijaga oleh 19 malaikat. Pada ayat-ayat berikut ini ditegaskan pula bahwa jumlah yang 19 malaikat itu hanyalah merupakan cobaan dan ujian bagi orang yang kafir. Akan tetapi, bagi orang yang beriman, keterangan serupa itu akan menambah keimanannya. Ayat-ayat selanjutnya mengatakan Saqar itu sebagai bencana besar untuk peringatan bagi manusia.",
        "theme_group": "BALASAN BAGI ORANG YANG MENERIMA\nDAN MENOLAK DAKWAH",
        "kosakata": "Kosakata: \n1.\tJun\u016bda Rabbika  \u062c\u064f\u0646\u064f\u0648\u0652\u062f\u064e \u0631\u064e\u0628\u0651\u0650\u0643\u064e  (al-Mudda\u1e61\u1e61ir/74: 31).\n\tKata jun\u016bd adalah bentuk jamak dari jund, terambil dari kata janad yang berarti himpunan sesuatu yang kasar dan padat. Tanah yang padat dinamai janad, karena kepadatannya akibat perpaduan bagian-bagian kecil dari tanah tersebut menjadikannya kokoh. Dari pengertian tersebut berkembang arti  jundiyy menjadi pengikut yang membantu mengokohkan yang diikutinya, atau yang kemudian populer dalam arti tentara.\n\tDari pengertian di atas maka jun\u016bda rabbika dalam ayat 31 Surah al-Mudda\u1e61\u1e61ir ini berarti tentara-tentara Tuhanmu, yakni makhluk-makhluk yang dijadikan Allah sebagai alat-alat yang kokoh dan terpadu guna menghadapi musuh-musuhnya serta melaksanakan kehendak-kehendaknya. \n\tKata jun\u016bd dalam berbagai bentuknya disebutkan 30 kali dalam            Al-Qur\u2019an dan semuanya berarti tentara.\n2.\tAl-Qamar \u0627\u0644\u0652\u0642\u064e\u0645\u064e\u0631  (al-Mudda\u1e61\u1e61ir/74: 32)\n\tKata al-qamar yang berarti bulan terambil  dari akar kata qamira- yaqmaru-qamaran yang berarti sangat putih. Bulan dinamai qamar karena cahayanya tampak keputih-putihan dan mengalahkan cahaya bintang-bintang. Kata al-qamar oleh orang Arab digunakan untuk salah satu satelit alami yang  mengitari bumi serta memantulkan cahaya matahari sehingga terlihat di waktu malam. Dinamai demikian setelah berlalu tiga malam pertama pada awal setiap bulan karena malam pertama sampai dengan malam ketiga, mereka namakan dengan hil\u0101l.\n\tKata al-qamar disebutkan 26 kali dalam Al-Qur\u2019an dalam bentuk ma\u2018rifah, dan 1 kali dalam bentuk nakirah. Dari 27 kali disebutkan dalam Al-Qur\u2019an itu, semuanya berarti bulan.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}