{
    "id": 5827,
    "surah_id": 81,
    "ayah": 27,
    "page": 586,
    "quarter_hizb": 59.25,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0627\u0650\u0646\u0652 \u0647\u064f\u0648\u064e \u0627\u0650\u0644\u0651\u064e\u0627 \u0630\u0650\u0643\u0652\u0631\u064c \u0644\u0651\u0650\u0644\u0652\u0639\u0670\u0644\u064e\u0645\u0650\u064a\u0652\u0646\u064e\u06d9",
    "latin": "In huwa ill\u0101 \u017cikrul lil-\u2018\u0101lam\u012bn(a).",
    "translation": "(Al-Qur\u2019an) itu tidak lain, kecuali peringatan bagi semesta alam, ",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 81,
        "arabic": "\u0627\u0644\u062a\u0651\u0643\u0648\u064a\u0631",
        "latin": "At-Takw\u012br",
        "transliteration": "At-Takwir",
        "translation": "Penggulungan",
        "num_ayah": 29,
        "page": 586,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Al-Qur\u2019an itu tidak lain adalah peringatan bagi seluruh alam. Al-Qur\u2019an menjelaskan segala sesuatu untuk kebaikan manusia, baik urusan dunia maupun akhirat; mereka yang berbuat baik akan mendapat pahala dan surga, dan yang berbuat jahat akan mendapat dosa dan neraka.",
        "tahlili": "Kemudian Allah menyatakan bahwa Al-Qur\u2019an itu tiada lain hanya peringatan bagi semesta alam, bagi mereka yang mempunyai hati cenderung kepada kebaikan. Namun demikian, tidak semua manusia dapat mengambil manfaat dari Al-Qur\u2019an ini. Yang mengambil manfaat ialah siapa yang mau menempuh jalan yang lurus. Adapun orang yang menyimpang dari jalan itu, maka ia tidak dapat mengambil manfaat dari peringatan Al-Qur\u2019an.",
        "intro_surah": "Surah at-Takw\u012br terdiri dari 29 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah al-Lahab.\nKata at-Takw\u012br yang menjadi nama bagi surah ini adalah kata asal (ma\u1e63dar) dari kata kerja kuwwirat (digulung) yang terdapat pada ayat pertama surah ini.\n\nPokok-pokok Isinya:\nKeguncangan-keguncangan yang terjadi pada hari Kiamat; pada hari Kiamat setiap jiwa akan mengetahui apa yang telah dikerjakannya waktu di dunia; Al-Qur\u2019an adalah firman Allah yang disampaikan oleh Jibril a.s.; penegasan atas kenabian Muhammad saw; Al-Qur\u2019an sumber petunjuk bagi umat manusia yang menginginkan hidup lurus; suksesnya manusia dalam mencatat kehidupan yang lurus itu tergantung kepada taufik dari Allah.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tSama-sama menerangkan tentang huru-hara pada hari Kiamat.\n2.\tSama-sama menerangkan bahwa manusia pada hari Kiamat terbagi dua.\n3.\tPada Surah \u2018Abasa, Allah menegur Muhammad saw, sedang dalam at-Takw\u012br, Allah menegaskan bahwa Muhammad saw adalah seorang rasul yang mulia.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menjelaskan kedahsyatan hari Kiamat, dan menerangkan bahwa manusia ketika itu melihat amal perbuatannya di dunia sebagai suatu fakta kenyataan dan dapat membedakan mana amal perbuatan yang diterima dan mana yang ditolak. Pada ayat-ayat berikut ini, Allah menjelaskan bahwa apa-apa yang disampaikan oleh Muhammad Rasulullah saw, yaitu Al-Qur\u2019an yang diturunkan kepadanya, adalah ayat-ayat yang jelas memberi petunjuk kepada jalan kebahagiaan. Apa-apa yang dituduhkan oleh orang-orang musyrik Mekah yang mengatakan bahwa Muhammad itu hanya seorang tukang sihir, orang gila, pendusta, atau penyair, adalah dusta yang timbul karena rasa permusuhan, kedengkian, dan kesombongan mereka.",
        "theme_group": "MUHAMMAD ADALAH SEORANG RASUL YANG \nDITURUNKAN KEPADANYA AL-QUR\u2019AN ",
        "kosakata": "Kosakata:\n1.\tAl-Khunnas \u0627\u0644\u0652\u062e\u064f\u0646\u0651\u064e\u0633\u0650 (at-Takw\u012br/81: 15)\nAl-Khunnas artinya bintang-bintang yang bercahaya. Pada siang hari, bintang-bintang itu memang tidak kelihatan, tetapi pada malam hari tampak jelas menerangi dan menghiasi langit yang luas. Pada ayat 15, Allah bersumpah dengan bintang-bintang di langit yang bersinar terang. Dalam \u2018ilmu ma\u2018\u0101ni sebagai bagian dari \u2018ilmu bal\u0101gah untuk menghadapi orang-orang atau mukh\u0101\u1e6dab yang tidak percaya, perlu menggunakan kalimat yang mengandung taukid lebih dari satu, dan kalimat sumpah adalah taukid yang kuat. Jadi, karena orang-orang kafir Mekah tidak percaya pada adanya hari kebangkitan, Allah sering menggunakan bentuk qasam atau sumpah untuk meyakinkan mereka. Pada ayat 15 ini, Allah berfirman dalam bentuk sumpah dengan bintang-bintang, karena bintang-bintang sangat dikagumi oleh manusia terutama para kafilah di padang pasir. Di samping memberi penerangan perjalanan di padang pasir maupun di tengah lautan, bintang-bintang juga memberi petunjuk tentang waktu, arah yang harus dituju, maupun peredaran musim dan sebagainya.\n2.\tAl-Kunnas \u0627\u064e\u0644\u0652\u0643\u064f\u0646\u0651\u064e\u0633\u0650 (at-Takw\u012br/81: 16)\nSecara kebahasaan kata al-kunnas adalah bentuk jamak dari al-k\u0101nisah yang berarti bintang atau bintang yang berjalan. Dalam konteks ayat ini, al-kunnas menjadi sifat bagi bintang. Di sini Allah bersumpah demi bintang-bintang yang beredar atau berjalan.\n3.\t\u2018As\u2018asa \u0639\u064e\u0633\u0652\u0639\u064e\u0633\u064e (at-Takw\u012br/81: 17)\nSecara kebahasaan kata \u2018as\u2018asa merupakan bentuk kata kerja (fi\u2018il m\u0101\u1e0d\u012b) yang berarti malam yang mulai gelap. Dalam konteks ayat ini, Allah bersumpah demi malam yang mulai gelap.\n\n4.\tBi \u1e0can\u012bn \u0628\u0650\u0636\u064e\u0646\u0650\u064a\u0652\u0646\u064d (at-Takw\u012br/81: 24)\nSecara kebahasaan kata bi \u1e0dan\u012bn terdiri dari dua suku kata, yaitu kata bi merupakan z\u0101\u2019idah (tambahan) yang, menurut sebagian mufasir, berfungsi sebagai penegasan, dan kata \u1e0dan\u012bn yang berarti orang yang kikir atau bakhil. Dengan demikian, kata bi \u1e0dan\u012bn di sini bermakna Allah menegaskan bahwa Nabi Muhammad bukanlah seorang yang enggan (bakhil) untuk menerangkan yang gaib, yaitu bertemu Malaikat Jibril dan menerima wahyu darinya.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}