{
    "id": 6003,
    "surah_id": 89,
    "ayah": 10,
    "page": 593,
    "quarter_hizb": 60,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0648\u064e\u0641\u0650\u0631\u0652\u0639\u064e\u0648\u0652\u0646\u064e \u0630\u0650\u0649 \u0627\u0644\u0652\u0627\u064e\u0648\u0652\u062a\u064e\u0627\u062f\u0650\u06d6",
    "latin": "Wa fir\u2018auna \u017cil-aut\u0101d(i). ",
    "translation": "dan Fir\u2018aun yang mempunyai pasak-pasak (bangunan yang besar)",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 89,
        "arabic": " \u0627\u0644\u0641\u062c\u0631",
        "latin": "Al-Fajr",
        "transliteration": "Al-Fajr",
        "translation": "Fajar",
        "num_ayah": 30,
        "page": 593,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "dan tidakkah kamu juga memperhatikan azab Allah kepada Fir\u2019aun yang mempunyai pasak-pasak? Allah mengazabnya meski ia mampu membangun piramida-piramida yang besar dan mempunyai bala tentara yang banyak.",
        "tahlili": "Allah juga telah menghancurkan Fir\u2018aun. Ia terkenal sebagai raja yang zalim bahkan memandang dirinya tuhan bangsa Mesir. Bangsa ini di bawah Fir\u2018aun juga telah mencapai peradaban yang tinggi, di antara buktinya adalah kemampuan mereka membangun piramid-piramid yang merupakan salah satu keajaiban dunia sampai sekarang. Mereka juga telah memiliki angkatan bersenjata yang besar. Akan tetapi, semuanya itu juga sudah dihancurleburkan Allah sehingga sekarang mereka hanya tinggal nama untuk dikenang.",
        "intro_surah": "Surah ini terdiri dari 30 ayat, termasuk kelompok surah Makkiyyah, turun sebelum Surah a\u1e0d-\u1e0cuh\u0101 dan sesudah Surah al-F\u012bl. Nama al-Fajr diambil dari kata al-fajr yang terdapat pada ayat pertama surah ini yang artinya \u201cfajar\u201d.\n\nPokok-pokok Isinya:\nAllah bersumpah dengan fajar dan malam setiap hari atau hari-hari tertentu untuk menekankan bahwa apa dan siapa pun di alam ini tidak akan abadi. Contohnya adalah beberapa umat terdahulu yang dihancurkan Allah karena kedurhakaan mereka walaupun mereka begitu kuat dan perkasa. Peristiwa-peristiwa itu hendaknya menjadi peringatan bagi kaum kafir Mekah bahwa bila mereka tetap membangkang, mereka dapat saja dihancurkan oleh Allah seperti umat-umat itu. Manusia secara pribadi juga demikian, mereka akan mati, kemudian akan menjalani kehidupan yang abadi di akhirat dalam keadaan bahagia atau sengsara. Oleh karena itu, manusia jangan terlalu cinta harta, sebab kecukupan materi di dunia belum tentu merupakan pertanda bahwa Allah mencintainya. Yang diperlukan adalah mencintai anak yatim, membantu orang miskin, dan tidak memakan harta pusaka yang tidak menjadi haknya. Orang yang bersih dari dosa akan dipersilakan oleh Allah untuk memasuki surga-Nya.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AL-G\u0100SYIYAH\nDENGAN SURAH AL-FAJR\n\n1. \tDalam Surah al-G\u0101syiyah, Allah menyebutkan tentang orang yang pada hari Kiamat tergambar di mukanya kehinaan, dan tentang orang yang bercahaya wajahnya. Sedang pada Surah al-Fajr disebutkan beberapa kaum yang mendustakan dan berbuat durhaka, sebagai contoh dari orang-orang yang tergambar di muka mereka kehinaan dan azab yang ditimpakan kepada mereka di dunia. Disebutkan pula orang yang berjiwa mu\u1e6dmainnah, mereka itulah orang-orang yang wajahnya bercahaya.\n2. \tDalam Surah al-G\u0101syiyah, Allah mengemukakan orang yang bercahaya wajahnya, sedang pada surah al-Fajr disebutkan orang yang berjiwa tenang di dunia karena iman dan takwanya yang di akhirat akan berseri-seri wajahnya.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah bersumpah dengan hari-hari penting atau hari-hari yang silih berganti untuk menunjukkan bahwa Ia Mahakuasa mematikan manusia dan menghidupkan mereka kembali sesudah mati. Pada ayat-ayat berikut ini, Allah memberikan contoh umat-umat terdahulu yang perkasa dan berkuasa, tetapi dihancurkan oleh Allah karena pembangkangan mereka. Peristiwa-peristiwa itu hendaknya menjadi pelajaran bagi orang-orang kafir agar beriman, karena peristiwa-peristiwa seperti itu dapat pula terjadi pada mereka. ",
        "theme_group": "KEHANCURAN UMAT-UMAT TERDAHULU\nKARENA KEDURHAKAAN MEREKA",
        "kosakata": "Kosakata: Irama \u017b\u0101til-\u2018Im\u0101d \u0627\u0644\u0652\u0639\u0650\u0645\u064e\u0627\u062f\u0650 \u0630\u064e\u0627\u062a\u0650 \u0627\u0650\u0631\u064e\u0645\u064e (al-Fajr/89: 7)\n\tKata-kata ini hanya terdapat dalam Surah al-Fajr/89: 7. Kata \u2018im\u0101d dalam Irama \u017c\u0101til-\u2018im\u0101d secara harfiah berarti kemah yang bertiang-tiang, yakni bangunan yang tinggi. \u017b\u0101tul-\u2018im\u0101d juga telah menjadi sebutan bagi kaum \u2018Ad, ras Arab sebelum kaum Samud (lihat kosakata \u2018Ad dan Samud). Menurut Ibnu Ka\u1e61\u012br, disebut \u017c\u0101tul-\u2018im\u0101d karena mereka tinggal di rumah-rumah dari batu yang ditopang oleh tiang-tiang yang kuat, dan pada zamannya mereka termasuk orang-orang yang berperawakan tegap dan garang. Demikian juga pendapat beberapa mufasir yang lain. Ada dua periode \u2018Ad, yaitu \u2018Ad pertama dan \u2018Ad kedua dengan menyebutkan silsilah mereka sampai kepada Nabi Nuh. Ada anggapan bahwa kata-kata \u017c\u0101tul-\u2018im\u0101d merupakan suatu kesatuan kata sebagai istilah geografi Irama \u017c\u0101til-\u2018im\u0101d. Sementara Iram ialah nama kota purbakala kaum \u2018Ad di Arab bagian selatan, dan mereka dikenal sebagai ahli bangunan. Umumnya mufasir berpendapat bahwa Iram nama orang, nenek moyang kaum \u2018Ad. Pengertian \u201ctiang-tiang yang tinggi\u201d ditafsirkan sebagai sosok tubuh yang tinggi.\nDalam Tafsir Abdullah Yusuf Ali diungkapkan bahwa menurut para mufasir, Iram adalah nama eponim seorang pahlawan kaum \u2018Ad, dan \u201ctiang-tiang yang tinggi\u201d ditafsirkan dengan \u201csosok tubuh yang tinggi\u201d, dan memang sosok tubuh kaum \u2018Ad tinggi-tinggi. Kawasan selatan Jazirah Arab ini pernah menjadi sangat makmur dan kaya dengan puing-puing dan prasasti-prasasti. Bagi orang Arab sendiri, daerah itu menjadi sasaran yang selalu menarik. Pada zaman muawiyah, pernah ditemukan beberapa permata dalam reruntuhan di tempat itu. Belum lama ini telah ditemukan pula perunggu kepala singa dan sebuah perunggu talang air dengan prasasti Saba\u2019, terdapat di Najran.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}