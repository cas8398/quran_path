{
    "id": 5582,
    "surah_id": 75,
    "ayah": 31,
    "page": 578,
    "quarter_hizb": 58.5,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u0641\u064e\u0644\u064e\u0627 \u0635\u064e\u062f\u0651\u064e\u0642\u064e \u0648\u064e\u0644\u064e\u0627 \u0635\u064e\u0644\u0651\u0670\u0649\u06d9",
    "latin": "Fal\u0101 \u1e63addaqa wa l\u0101 \u1e63all\u0101.",
    "translation": "Dia tidak membenarkan (Al-Qur\u2019an dan Rasul) dan tidak melaksanakan salat. ",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 75,
        "arabic": " \u0627\u0644\u0642\u064a\u0670\u0645\u0629",
        "latin": "Al-Qiy\u0101mah",
        "transliteration": "Al-Qiyamah",
        "translation": "Hari Kiamat",
        "num_ayah": 40,
        "page": 577,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "31-33. Ayat ini menjelaskan beberapa sebab mengapa para pendurhaka diseret ke neraka. karena dia dahulu tidak mau membenarkan Al-Qur\u2019an dan Rasul dan juga tidak mau melaksanakan salat, tetapi justru dia mendustakan Rasul dan berpaling dari kebenaran. Bukan hanya sampai di situ, bahkan kemudian dia pergi kepada keluarganya dengan sombong. Dapat juga diartikan bahwa ayat 31-32, menggambarkan hubungannya yang buruk terhadap Allah, sedangkan ayat 33 menggambarkan buruknya hubungan sosial.",
        "tahlili": "Ayat-ayat ini menerangkan bahwa orang kafir itu tidak mau membenarkan rasul, dan berpaling dari kebenaran serta tidak mau mengerjakan salat. Ia selalu mendustakan Rasulullah dan Al-Qur\u2019an, dan tidak mau mengesakan Allah. Ia tetap menyekutukan-Nya dan meyakini bahwa Tuhan itu berbilang. Ia juga tidak mau mengerjakan kewajiban-kewajiban yang dibebankan kepadanya, dan selalu menentang dan berpaling dari perintah Tuhan, serta terpengaruh oleh kesenangan duniawi.",
        "intro_surah": "Surah al-Qiy\u0101mah terdiri atas 40 ayat, 199 kata dan 652 huruf. Termasuk kelompok surah Makkiyyah, dan diturunkan sesudah Surah al-Q\u0101ri\u2018ah. Nama al-Qiy\u0101mah (Hari Kiamat) diambil dari kata al-qiy\u0101mah yang terdapat pada ayat pertama. Dinamakan al-Qiy\u0101mah karena sebagian besar surah ini menceritakan kedahsyatan hari Kiamat. Saat pahala dan siksaan yang dialami manusia tiada batasnya. Pada hari itu manusia menyesal karena sedikitnya perbuatan baik yang telah dikerjakannya.\n\nPokok-pokok Isinya:\n1. \tAllah memastikan kedatangan hari Kiamat itu, disertai gambaran sekitar huru-hara yang terjadi pada masa itu.\n2. \tSurah ini menyebutkan sebagian dari jaminan Allah terhadap kemurnian Al-Qur\u2019an, yakni ayat-ayatnya terpelihara dengan baik dalam dada Nabi, sehingga beliau tidak lupa sedikit pun tentang urutan dan pembacaannya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tSurah al-Mudda\u1e61\u1e61ir menerangkan bahwa bagaimanapun keterangan-keterangan dikemukakan kepada orang-orang kafir itu, namun mereka tetap tidak percaya. Mereka tidak merasa takut dan gentar sedikit pun dengan hari kebangkitan itu, karena mereka tidak mengimaninya. Maka pada ayat ini dalil-dalil tentang hari akhirat itu disebutkan lebih lengkap lagi guna menyempurnakan keterangan yang terdapat di dalam Surah al-Mudda\u1e61\u1e61ir. Di sini disebutkan tentang sifat-sifat hari Kiamat itu, kedahsyatannya dan keadaan manusia pada hari itu. Sebelumnya Allah menerangkan tentang dicabutnya roh manusia pada saat ia meninggal dunia, dan masalah asal mula kejadian manusia yang diciptakan Allah dari setetes air yang kotor (mani).\n2. \tSurah al-Mudda\u1e61\u1e61ir mengungkapkan bahwa orang-orang kafir mendustakan Al-Qur\u2019an dan menganggapnya sebagai perkataan manusia biasa, sedang pada Surah al-Qiy\u0101mah ini, Allah menjamin Al-Qur\u2019an dalam ingatan Nabi dan mengajarkan bacaannya.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, diterangkan dua golongan manusia di akhirat: orang mukmin yang berwajah cerah dan orang kafir yang berwajah muram karena takut kepada azab Allah. Pada ayat-ayat berikut ini, diterangkan bahwa orang kafir ketika sakratulmaut baru sadar akan berpisah dengan dunia dan digiring menghadap Allah.",
        "theme_group": "KEADAAN MANUSIA SAAT SAKRATULMAUT",
        "kosakata": "Kosakata:\n1. At-Tar\u0101q\u012b  \u0627\u0644\u062a\u0651\u064e\u0631\u064e\u0627\u0642\u0650\u0649 (al-Qiy\u0101mah/75: 26)\n\tAt-Tar\u0101q\u012b merupakan bentuk jamak dari kata tarquwah, yang artinya lubang yang terdapat di kerongkongan untuk pernapasan dan saluran makanan. Pada ayat ini, kata ini dikaitkan dengan fenomena yang dialami manusia ketika roh dicabut dan dipisahkan dari raganya. Dalam proses pencabutan itu, roh merambat naik dari kaki ke atas, dan fase akhir dari kehidupan seseorang ditandai dengan sampainya roh ke tar\u0101q\u012b tersebut. Pada saat seperti ini, seseorang yang mengalaminya disebut sedang berada dalam keadaan sakaratulmaut, dan pernapasannya akan terdengar bergetar, yang dalam bahasa Arab disebut yugargir. Inilah batas akhir dari diterimanya tobat seseorang. \n2. Sud\u0101n \u0633\u064f\u062f\u064b\u0649 (al-Qiy\u0101mah/75: 36)\n\tSud\u0101 maknanya adalah diremehkan atau disia-siakan. Ayat ini mengisyaratkan bahwa manusia itu tidak akan diremehkan atau disia-siakan. Makna yang demikian memberikan pemahaman bahwa manusia itu merupakan makhluk istimewa dan bukan sesuatu yang diremehkan di sisi Allah. Manusia adalah makhluk terhormat yang tidak akan dibiarkan begitu saja. Pemahaman demikian membawa pada kesimpulan bahwa tujuan penciptaan manusia itu adalah sedemikian pentingnya, sehingga mereka mendapatkan segala kasih sayang dan perhatian utama dari Allah. Selain itu, demi menjaga keseimbangan dalam prilaku dan perbuatan, manusia yang bukan makhluk remeh itu akan dibangkitkan kelak setelah kematiannya. Tujuan penciptaan manusia adalah sebagai khalifah dan sekaligus untuk beribadah kepada Allah. Ini adalah tujuan yang mulia, karena mengemban misi dari-Nya untuk mengelola bumi. Seandainya manusia diciptakan tanpa tujuan, maka penciptaan dan kejadiannya tentu tidak perlu dengan proses yang rumit dan berfase-fase. Tuhan tentu tidak melakukan semua ini dengan sia-sia. Oleh karena itulah, manusia bukan makhluk yang akan diremehkan atau disia-siakan.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}