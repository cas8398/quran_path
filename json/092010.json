{
    "id": 6068,
    "surah_id": 92,
    "ayah": 10,
    "page": 595,
    "quarter_hizb": 60.25,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0641\u064e\u0633\u064e\u0646\u064f\u064a\u064e\u0633\u0651\u0650\u0631\u064f\u0647\u0657 \u0644\u0650\u0644\u0652\u0639\u064f\u0633\u0652\u0631\u0670\u0649\u06d7",
    "latin": "Fa sanuyassiruh\u016b lil-\u2018usr\u0101.   ",
    "translation": "Kami akan memudahkannya menuju jalan kesengsaraan.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 92,
        "arabic": " \u0627\u0644\u0651\u064a\u0644",
        "latin": "Al-Lail",
        "transliteration": "Al-Lail",
        "translation": "Malam",
        "num_ayah": 21,
        "page": 595,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "maka akan Kami mudahkan baginya jalan menuju kesukaran dan kesengsaraan. Kami tutup hatinya dari keinginan untuk berbuat kebajikan dan Kami tahan langkahnya untuk taat kepada Kami.",
        "tahlili": "Sebaliknya, ada manusia yang bertingkah laku sebaliknya. Ia bakhil, pelit, tidak mau menolong antar sesama, apalagi mengeluarkan kewajibannya yaitu zakat. Di samping itu, ia sudah merasa cukup segala-galanya. Oleh karena itu, ia merasa tidak memerlukan orang lain bahkan Allah. Akibatnya, ia sombong dan tidak mengakui nikmat-nikmat Allah yang telah ia terima dan tidak mengharapkan nikmat-nikmat itu. Akibatnya ia tidak mengindahkan aturan-aturan Allah. Orang itu akan dimudahkan Allah menuju kesulitan, baik kesulitan di dunia maupun di akhirat. Kesulitan di dunia misalnya kejatuhan, penyakit, kecelakaan, musibah, dan sebagainya. Kesulitan di akhirat adalah ketersiksaan yang puncaknya adalah neraka.\nManusia, bila sudah mati tanpa memiliki amal dan kemudian masuk neraka di akhirat, maka harta benda dan kekayaan mereka tidak berguna apa pun. Hal itu karena harta itu tidak akan bisa digunakan untuk menebus dosa-dosa mereka.",
        "intro_surah": "Surah al-Lail terdiri dari 21 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah surah al-A\u2018l\u0101 sebelum Surah al-Fajr.\nSurah ini dinamai al-Lail (malam), diambil dari kata al-lail yang terdapat pada ayat pertama surah ini.\n\nPokok-pokok Isinya:\nSurah ini juga dimulai dengan sumpah-sumpah Allah dengan makhluk-makhluk-Nya yang bertolak belakang sifat-sifatnya, yaitu malam dan siang serta laki-laki dan perempuan. Isi sumpah (muqsam \u2018alaih) Allah juga berkenaan dengan sifat manusia berkenaan kekayaan yang bermacam-macam, yaitu ada yang mau menggunakannya untuk membantu orang lain dan ada yang tidak mau. Menggunakannya untuk membantu orang lain akan membawa mereka masuk ke dalam surga. Sedangkan kikir akan membawa ke dalam neraka.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH ASY-SYAMS\nDENGAN SURAH AL-LAIL\n\nSurah asy-Syams menginformasikan adanya dua golongan manusia, yaitu yang menyucikan dirinya dengan berbuat baik dan bebas dari dosa, dan orang mengotori dirinya dengan melakukan dosa-dosa. Dalam Surah al-Lail juga diterangkan dua golongan manusia. Bila dalam Surah asy-Syams penggolongan itu berdasarkan usaha manusia membersihkan dirinya dengan melakukan perbuatan-perbuatan baik dan menjauhi perbuatan-perbuatan dosa, dalam Surah al-Lail ini penggolongan itu berdasarkan aktualisasi yang konkrit perbuatan baik itu, yaitu kesediaan mengorbankan harta atau ketidak sediaannya. Hal itu mengandung arti bahwa salah satu cara membersihkan diri itu adalah dengan mengorbankan harta tersebut.",
        "munasabah_prev_theme": "Dalam ayat yang lalu, Allah menegaskan bahwa perbuatan dan tingkah laku manusia itu berbeda-beda. Dalam ayat-ayat berikut dijelaskan perbedaan tingkah laku itu berkenaan dengan harta.",
        "theme_group": "DUA TINGKAH LAKU MANUSIA YANG BERTENTANGAN \nBERKENAAN DENGAN KEKAYAAN ",
        "kosakata": "Kosakata:\n1.\tBakhila \u0628\u064e\u062e\u0650\u0644\u064e (al-Lail/92: 8)\nKata bakhila berasal dari kata bakhila-yabkhalu-bukhlan wa bakhalan yang berarti menahan sesuatu yang tidak berhak ditahan, antonim dari kata al-karam yang berarti pemurah. Al-Bakh\u012bl adalah sebutan untuk orang yang sangat kikir. Bentuk jamaknya adalah bukhal\u0101'. Ada dua macam bukhl yaitu bakhil untuk dirinya sendiri dan bakhil untuk orang lain (an-Nis\u0101\u2019/4: 37). Kata ini dengan berbagai bentuk turunannya terulang sebanyak 12 kali dalam Al-Qur\u2019an. Kesemuanya menunjukkan pada makna kikir. Ayat-ayat ini menjelaskan tentang orang-orang yang kikir, enggan memberi, dan merasa dirinya cukup, tidak membutuhkan sesuatu sehingga mengabaikan orang lain atau mengabaikan tuntunan Allah dan rasul-Nya serta mendustakan kalimat-Nya. Allah akan memudahkan baginya kesukaran, yaitu menyiapkan baginya aneka jalan untuk menuju kepada hal-hal yang mengantarkannya kepada kesulitan dan kecelakaan yang abadi.\n2.\tIstagn\u0101 \u0627\u0650\u0633\u0652\u062a\u064e\u063a\u0652\u0646\u064e\u0649 (al-Lail/92: 8)\nKalimat istagn\u0101 merupakan bentuk fi\u2018il m\u0101\u1e0d\u012b dari kata ganiyy yang berarti merasa berkecukupan. Kata ini berasal dari akar kata yang terdiri dari huruf gain, n\u016bn, dan y\u0101\u2019. Ada beberapa macam pengertian dari kata ini yaitu pertama, tidak membutuhkan sesuatu sama sekali atau tidak menggantungkan kebutuhannya kepada yang lain. Sifat ini hanyalah untuk Allah. Dari sini lahir kata g\u0101niyah sebutan untuk wanita yang tidak kawin dan merasa berkecukupan hidup di rumah orang tuanya atau merasa cukup hidup sendirian tanpa bersuami. Kedua, sedikit sekali kebutuhannya atau merasa cukup. Ketiga, yang terpenuhi segala kebutuhannya.\nDi dalam Al-Qur\u2019an, kata istagn\u0101 terulang sebanyak 4 kali. Kesemuanya menunjuk pada makna merasa cukup, satu kali dinisbahkan kepada Allah (at-Tag\u0101bun/64: 6) bahwa Allah tidak memerlukan mereka dan 3 kali dinisbahkan kepada manusia dalam arti negatif yaitu mereka merasa serba cukup, tidak membutuhkan pertolongan Allah. Dalam Al-Qur\u2019an dan hadis, kalimat ini tidak selalu diartikan dengan banyaknya harta kekayaan. Dalam hadis yang cukup populer, Nabi saw mengatakan bahwa gin\u0101 (kekayaan) tidak dinilai dengan banyaknya harta benda tetapi kekayaan yang sebenarnya adalah kekayaan hati.\n\tKebalikan dari ayat-ayat sebelumnya yang berkenaan dengan akibat baik yang akan diterima bagi siapa yang memberi dan bertakwa, maka ayat ini menjelaskan bahwa bagi orang-orang bakhil lagi kikir dan merasa dirinya serba cukup sehingga tidak membutuhkan Allah dan orang lain, telah disiapkan kesulitan dan kesengsaraan yang abadi.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}