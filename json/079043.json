{
    "id": 5755,
    "surah_id": 79,
    "ayah": 43,
    "page": 584,
    "quarter_hizb": 59,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0641\u0650\u064a\u0652\u0645\u064e \u0627\u064e\u0646\u0652\u062a\u064e \u0645\u0650\u0646\u0652 \u0630\u0650\u0643\u0652\u0631\u0670\u0649\u0647\u064e\u0627\u06d7",
    "latin": "F\u012bma anta min \u017cikr\u0101h\u0101.",
    "translation": "Untuk apa engkau perlu menyebutkan (waktu)-nya?",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 79,
        "arabic": " \u0627\u0644\u0646\u0651\u0670\u0632\u0639\u0670\u062a",
        "latin": "An-N\u0101zi\u2018\u0101t",
        "transliteration": "An-Nazi\u2018at",
        "translation": "Yang Mencabut Dengan Keras",
        "num_ayah": 46,
        "page": 583,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Untuk apa engkau perlu menyebutkan waktu-nya kepada mereka, sedang engkau sendiri tidak mengetahui hal itu?",
        "tahlili": "Dalam ayat ini, Allah menanyakan apakah Nabi Muhammad akan menyebutkan waktu Kiamat itu? Padahal tugasnya hanya sekadar memberi peringatan sehingga tidak ada kewenangan untuk menyebutkan tentang kedatangan hari kebangkitan. Waktu datangnya hari Kiamat tetap merupakan rahasia Allah. Nabi sendiri tidak mengetahui tentang waktu kedatangannya, sebagaimana difirmankan Allah dalam Al-Qur\u2019an:\n\u0627\u0650\u0646\u0652 \u0639\u064e\u0644\u064e\u064a\u0652\u0643\u064e \u0627\u0650\u0644\u0651\u064e\u0627 \u0627\u0644\u0652\u0628\u064e\u0644\u0670\u063a\u064f \u0664\u0668\nKewajibanmu tidak lain hanyalah menyampaikan (risalah). (asy-Sy\u016br\u0101/42: 48)\n\nDalam sebuah hadis yang diriwayatkan oleh \u2018Umar bin Kha\u1e6d\u1e6d\u0101b, ketika Nabi ditanya tentang kapan datangnya hari Kiamat, beliau menjawab:\n\u0645\u064e\u0627 \u0627\u0644\u0652\u0645\u064e\u0633\u0652\u0626\u064f\u0648\u0652\u0644\u064f \u0628\u0650\u0623\u064e\u0639\u0652\u0644\u064e\u0645\u064e \u0645\u0650\u0646\u064e \u0627\u0644\u0633\u0651\u064e\u0627\u0626\u0650\u0644\u0650 (\u0631\u0648\u0627\u0647 \u0645\u0633\u0644\u0645 \u0639\u0646 \u0639\u0645\u0631 \u0628\u0646 \u0627\u0644\u062e\u0637\u0627\u0628) \nOrang yang ditanya tidaklah lebih mengetahui daripada orang yang bertanya. (Riwayat Muslim dari \u2018Umar bin al-Kha\u1e6d\u1e6d\u0101b)\nAllah tetap merahasiakan waktu datangnya hari Kiamat mempunyai hikmah yang besar, yaitu supaya manusia selalu mempersiapkan diri setiap saat dengan banyak-banyak berbuat kebaikan dan selalu menghindari perbuatan jahat.",
        "intro_surah": "Surah an-N\u0101zi\u2018\u0101t terdiri dari 46 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah an-Naba\u2019.\nNama an-N\u0101zi\u2018\u0101t (Malaikat-malaikat yang mencabut) diambil dari kata an-N\u0101zi\u2018\u0101t yang terdapat pada ayat pertama surah ini.\nSurah ini dinamai pula dengan as-S\u0101hirah yang diambil dari ayat 14, dan dinamai juga a\u1e6d-\u1e6c\u0101mmah yang diambil dari ayat 34.\n\nPokok-pokok Isinya:\n1.\tKeimanan:\nPenegasan Allah tentang adanya hari Kiamat dan sikap orang-orang musyrik terhadapnya; manusia dibagi dua golongan di akhirat; manusia tidak dapat mengetahui kapan terjadinya saat Kiamat.\n2.\tKisah:\nKisah Nabi Musa dengan Fir\u2018aun.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AN-NABA\u2019\nDENGAN SURAH AN-N\u0100ZI\u2018\u0100T\n\n1. \tSurah an-Naba\u2019 menerangkan ancaman Allah terhadap sikap orang-orang musyrik yang mengingkari adanya hari kebangkitan, serta mengemukakan bukti-bukti adanya hari kebangkitan, sedangkan pada Surah an-N\u0101zi\u2018\u0101t, Allah bersumpah bahwa hari Kiamat yang mendahului hari kebangkitan itu pasti terjadi.\n2. \tSama-sama menerangkan huru-hara yang terjadi pada hari Kiamat dan hari kebangkitan.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menjelaskan bahwa pada hari Kiamat setiap manusia akan teringat pada apa yang telah dikerjakannya di dunia. Semuanya akan menyesal karena tidak atau terlalu sedikit berbuat kebaikan. Orang yang jahat akan masuk neraka dan orang yang baik akan masuk surga. Pada ayat-ayat berikut ini, Allah menegaskan bahwa tidak ada yang mengetahui kapan datangnya hari Kiamat karena merupakan rahasia yang hanya diketahui oleh Allah saja.",
        "theme_group": "HANYA ALLAH YANG MENGETAHUI\nDATANGNYA HARI KEBANGKITAN",
        "kosakata": "Kosakata: Murs\u0101h\u0101 \u0645\u064f\u0631\u0652\u0633\u0670\u0647\u0627 (an-N\u0101zi\u2018\u0101t/79: 42)\nMurs\u0101h\u0101 berasal dari fi\u2018il ras\u0101-yars\u016b-raswan wa rusuwwan yang artinya tetap, berlabuh, atau mendarat. Al-Murs\u0101 adalah isim maf\u2018\u016bl yang artinya dijadikan atau terjadi. Dapat pula merupakan isim mak\u0101n (tempat berlabuh atau pelabuhan). Ayy\u0101na murs\u0101h\u0101 artinya kapan terjadinya. Ayat 42 menerangkan tentang orang-orang kafir yang bertanya kepada Nabi Muhammad perihal hari Kiamat, kapan terjadinya? Kalimat tanya disini dalam \u2018ilmu bal\u0101gah tidak berarti yang sebenarnya yaitu ingin tahu kapan terjadinya hari kiamat, melainkan berarti taub\u012bkh yaitu mengejek. Orang-orang kafir memang tidak percaya pada adanya hari kebangkitan. Oleh karena itu, mereka selalu bertanya dengan nada mengejek, yaitu tidak mungkin ada hari kebangkitan, kapan terjadi hari kebangkitan atau hari Kiamat itu kapan? Padahal hari Kiamat dan hari kebangkitan itu pasti terjadi, dan waktunya adalah rahasia Allah sendiri.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}