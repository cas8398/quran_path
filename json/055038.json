{
    "id": 4939,
    "surah_id": 55,
    "ayah": 38,
    "page": 532,
    "quarter_hizb": 54,
    "juz": 27,
    "manzil": 7,
    "arabic": "\u0641\u064e\u0628\u0650\u0627\u064e\u064a\u0651\u0650 \u0627\u0670\u0644\u064e\u0627\u06e4\u0621\u0650 \u0631\u064e\u0628\u0651\u0650\u0643\u064f\u0645\u064e\u0627 \u062a\u064f\u0643\u064e\u0630\u0651\u0650\u0628\u0670\u0646\u0650 ",
    "latin": "Fa bi'ayyi \u0101l\u0101'i rabbikum\u0101 tuka\u017c\u017cib\u0101n(i).",
    "translation": "Maka, nikmat Tuhanmu manakah yang kamu dustakan (wahai jin dan manusia)?",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 55,
        "arabic": "\u0627\u0644\u0631\u0651\u062d\u0645\u0670\u0646",
        "latin": "Ar-Ra\u1e25m\u0101n",
        "transliteration": "Ar-Rahman",
        "translation": "Yang Maha Pengasih",
        "num_ayah": 78,
        "page": 531,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Maka, wahai manusia dan jin, nikmat Tuhanmu yang manakah yang kamu dustakan?",
        "tahlili": "Ayat ini Allah menantang jin dan manusia apakah mereka masih mendustakan nikmat-Nya. Allah mengabarkan segala sesuatu yang dapat mengakibatkan manusia menghindari segala kejahatan, sehingga akhirnya ia selamat dari ancaman, itulah nikmat Tuhan.",
        "intro_surah": "Surah ar-Ra\u1e25m\u0101n terdiri dari 78 ayat, termasuk kelompok surah Madaniyyah, diturunkan sesudah Surah ar-Ra\u2018d.\nDinamai ar-Ra\u1e25m\u0101n (Yang Maha Pemurah), diambil dari kata ar-ra\u1e25m\u0101n yang terdapat pada ayat pertama surah ini. Ar-Ra\u1e25m\u0101n adalah salah satu dari nama-nama Allah. Sebagian besar dari isi surah ini menerangkan kemurahan Allah kepada hamba-hamba-Nya, dengan memberikan nikmat-nikmat yang tidak terhingga kepada mereka baik di dunia maupun di akhirat nanti.\n\nPokok-pokok Isinya:\n1.\tKeimanan:\nAllah menciptakan manusia dan mengajar mereka berbicara; alam semesta tunduk kepada Allah; semua makhluk akan hancur kecuali Allah; Allah selalu sibuk bekerja mentadbirkan alam; seluruh alam merupakan nikmat Allah terhadap umat manusia; manusia diciptakan dari tanah dan jin dari api.\n2.\tHukum-hukum:\n\tKewajiban memenuhi ukuran, takaran, dan timbangan.\n3.\tLain-lain:\nManusia dan jin tidak akan mampu lari dari kekuasaan Allah, banyak umat manusia yang tidak mensyukuri nikmat Allah; memberitakan tentang keajaiban-keajaiban alam sebagai bukti kekuasaan Allah.",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tSurah al-Qamar menerangkan tentang adanya hari Kiamat dan apa yang akan dirasakan manusia yang baik di surga, dan manusia yang jahat di neraka. Surah ar-Ra\u1e25m\u0101n menerangkan secara lebih luas kenikmatan hidup di surga.\n2. \tSurah al-Qamar menerangkan azab yang ditimpakan kepada umat-umat terdahulu yang mendurhakai nabi-nabi mereka, Surah ar-Ra\u1e25m\u0101n menye-butkan berbagai nikmat Allah yang dilimpahkan-Nya kepada jin dan manusia sebagai hamba-hamba-Nya, agar mereka memilih beriman.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu Allah swt menjelaskan bahwa pada hari Kiamat jin dan manusia masing-masing akan menerima balasan sesuai dengan perbuatan-perbuatannya dan mereka tidak dapat menyelamatkan dirinya. Pada ayat-ayat berikut ini Allah swt menerangkan bahwa apabila tiba hari Kiamat, rusaklah peraturan alam seluruhnya: langit akan terbelah-belah dan akan merah warnanya, tidak akan melekat satu bagian dengan yang lain seperti minyak dengan air. Ketika itu akan tampak tanda-tanda orang-orang yang jahat dan akan terlihat jelas perbedaannya dengan orang lain. ",
        "theme_group": "GAMBARAN HARI KIAMAT",
        "kosakata": "Kosakata:\n1.\tAn-Naw\u0101\u1e63\u012b  \u0627\u0644\u0646\u0651\u064e\u0648\u064e\u0627\u0635\u0650\u064a\u0652 (ar-Ra\u1e25m\u0101n/55: 41)\nKata an-naw\u0101\u1e63\u012b merupakan bentuk jamak dari an-n\u0101\u1e63iyah, yang artinya ubun-ubun, atau tempat tumbuhnya rambut pada bagian puncak kepala. Selain makna secara bahasa, seperti yang telah diungkapkan, ada pula yang memahami kata ini dengan mengartikannya sebagai rambut yang tumbuh di bagian ubun-ubun tersebut. Makna seperti ini terdapat dalam pemahaman dari Surah al-\u2018Alaq/96: 15, yaitu bahwa orang yang tidak berhenti dari kegiatannya dalam mengganggu akan ditarik ubun-ubunnya (atau yang lebih tepat rambut yang tumbuh di tempat tersebut, karena menarik rambut lebih mudah dipahami ketimbang menarik ubun-ubun yang menyatu dan merupakan bagian dari kepala). Namun demikian makna apa saja yang dimaksud, pemahaman yang disimpulkan dari ayat ini adalah untuk menyatakan bahwa para pendurhaka itu akan dapat dikuasai secara penuh dan mudah di akhirat, sebagai balasan dari perbuatan mereka.\n2.\tAl-Aqd\u0101m \u0627\u0644\u0652\u0623\u064e\u0642\u0652\u062f\u064e\u0627\u0645 (ar-Ra\u1e25m\u0101n/55: 41) \nKata al-aqd\u0101m merupakan bentuk jamak dari al-qadam, yang artinya bagian bawah dari kaki. Kata ini disebut dalam ayat yang digandengkan dengan an-naw\u0101\u1e63\u012b untuk mengungkapkan keadaan seseorang secara keseluruhan. Kalau an-naw\u0101\u1e63\u012b menunjuk bagian paling atas dari jasmani manusia, maka al-aqd\u0101m menunjuk bagian paling bawahnya. Dengan pengungkapan ini, ayat tersebut mengisyaratkan bahwa manusia yang berbuat durhaka akan dikuasai secara keseluruhan dari jasmaninya dalam rangka menerima balasan. Penguasaan itu terjadi dengan mudah, sebagai-mana yang ditunjukkan oleh bentuk pasif dari kata kerja sebelumnya, yaitu fayu\u2019kha\u017cu, yang artinya maka diambil atau dipegang.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}