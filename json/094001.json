{
    "id": 6091,
    "surah_id": 94,
    "ayah": 1,
    "page": 596,
    "quarter_hizb": 60.5,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0627\u064e\u0644\u064e\u0645\u0652 \u0646\u064e\u0634\u0652\u0631\u064e\u062d\u0652 \u0644\u064e\u0643\u064e \u0635\u064e\u062f\u0652\u0631\u064e\u0643\u064e\u06d9",
    "latin": "Alam nasyra\u1e25 laka \u1e63adrak(a).",
    "translation": "Bukankah Kami telah melapangkan dadamu (Nabi Muhammad),",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 94,
        "arabic": "\u0627\u0644\u0634\u0651\u0631\u062d",
        "latin": "Asy-Syar\u1e25",
        "transliteration": "Asy-Syarh",
        "translation": "Pelapangan",
        "num_ayah": 8,
        "page": 596,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Wahai Nabi, bukankah Kami telah melapangkan dadamu? Kami telah menjadikanmu seorang nabi yang menerima syariat agama, berakhlak mulia, berwawasan luas, santun, dan sabar dalam menghadapi kepahitan hidup.",
        "tahlili": "Dalam ayat ini dinyatakan bahwa Allah telah melapangkan dada Nabi Muhammad dan menyelamatkannya dari ketidaktahuan tentang syariat. Nabi juga dirisaukan akibat kebodohan dan keras kepala kaumnya. Mereka tidak mau mengikuti kebenaran, sedang Nabi saw selalu mencari jalan untuk melepaskan mereka dari lembah kebodohan, sehingga ia menemui jalan untuk itu dan menyelamatkan mereka dari kehancuran yang sedang mereka alami.\nMaksud dari ayat ini adalah Allah telah membersihkan jiwa Nabi saw dari segala macam perasaan cemas, sehingga dia tidak gelisah, susah, dan gusar. Nabi juga dijadikan selalu tenang dan percaya akan pertolongan dan bantuan Allah kepadanya. Nabi juga yakin bahwa Dia yang menugasinya sebagai rasul, sekali-kali tidak akan membantu musuh-musuhnya.",
        "intro_surah": "Surah ini terdiri dari 8 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah a\u1e0d-\u1e0cu\u1e25\u0101. Nama asy-Syar\u1e25 (melapangkan) diambil dari kata alam nasyra\u1e25 yang terdapat pada ayat pertama, yang memberitakan tentang dibukanya hati Nabi Muhammad, dan kemudian disinari dan diisi dengan petunjuk, keimanan, dan hikmah.\n\nPokok-pokok Isinya:\nPenegasan tentang nikmat-nikmat Allah yang diberikan kepada Nabi Muhammad dan pernyataan Allah bahwa di samping kesukaran ada kemudahan. Oleh karena itu, Nabi diperintahkan agar tetap melakukan amal-amal saleh dan bertawakal kepada Allah.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH A\u1e0c-\u1e0cU\u1e24\u0100\nDENGAN SURAH ASY-SYAR\u1e24\n\n1. \tKedua surah ini sama-sama ditujukan kepada Nabi Muhammad. \u1e6c\u0101wus dan \u2018Umar bin \u2018Abdul \u2018Az\u012bz menyatakan bahwa Surah a\u1e0d-\u1e0cu\u1e25\u0101 dan Surah asy-Syar\u1e25 itu satu. Mereka membaca kedua surah tersebut dalam satu rakaat dan tidak membatasinya dengan ucapan basmalah. Akan tetapi, menurut riwayat yang mutawatir, a\u1e0d-\u1e0cu\u1e25\u0101 dan asy-Syar\u1e25 adalah dua surah meskipun terdapat hubungan arti antara keduanya.\n2. \tKedua surah ini sama-sama menerangkan nikmat-nikmat Allah dan memerintahkan Nabi untuk menyukurinya.",
        "munasabah_prev_theme": "Pada akhir ayat Surah a\u1e0d-\u1e0cu\u1e25\u0101 Allah memerintahkan Nabi Muhammad saw agar menolong dan memperhatikan anak yatim. Pada ayat-ayat berikut ini Allah memerintahkan kepada Nabi Muhammad saw agar membersihkan jiwanya sehingga Allah akan melapangkan dadanya.",
        "theme_group": "ANUGERAH ALLAH KEPADA NABI MUHAMMAD ",
        "kosakata": "Kosakata: \n1.\tWizraka \u0648\u0650\u0632\u0652\u0631\u064e\u0643\u064e (asy-Syar\u1e25/94: 2)\nKata wizraka terdiri dari dua kata yaitu wizr dan \u1e0dam\u012br mutta\u1e63il mukh\u0101\u1e6dabah \u201cka\u201d yang berarti kamu. Kata wizr berasal dari kata al-wazar yang berarti gunung yang dijadikan sebagai tempat berlindung. Kemudian kata ini digunakan untuk beban berat yang dipikul menyerupai gunung yang memberi kesan sesuatu yang besar dan berat.  Dari kata ini lahir kata waz\u012br misalnya yang menunjukkan pada pemikulan tugas dan tanggung jawab yang berat dari tuannya. Wizr juga diartikan dengan dosa, karena dengan dosa seseorang akan merasakan dalam jiwanya sesuatu yang sangat berat. Di samping itu, dosa akan menjadi sesuatu yang sangat berat dipikul pada hari Kiamat nanti. Bentuk jamaknya adalah auz\u0101r. Auz\u0101rul-\u1e25arb adalah alat-alat berat yang digunakan dalam peperangan. Al-Muw\u0101zarah juga berarti menolong urusan seseorang, seperti doa Nabi Musa dalam Surah \u1e6c\u0101h\u0101/20: 29, yang menginginkan Nabi Harun sebagai waz\u012br-nya (penolong). Kata ini dengan berbagai bentuk derivasinya terulang sebanyak 27 kali dalam Al-Qur\u2019an. \n\tMaksud ayat ini adalah Allah telah menanggalkan beban yang memberatkan punggung Nabi Muhammad dan melapangkan dadanya. Menurut sebagian ulama, beban ini adalah rasa cemas dan khawatir yang menghinggapi jiwa Rasul terkait dengan risalah dakwah yang beliau emban. Kata ini memberi kesan bahwa Rasulullah senantiasa memikirkan umatnya yang terus menghambat dan menghalang-halangi dakwahnya. Ini merupakan suatu beban (wizr) yang dipikul Muhammad.\n2.\tAnqa\u1e0da \u0627\u064e\u0646\u0652\u0642\u064e\u0636\u064e (asy-Syar\u1e25/94: 3)\nLafal anqa\u1e0da berasal dari kata naqa\u1e0da-yanqu\u1e0du-naq\u1e0d yang berarti mengurai sesuatu yang sudah terikat atau merusak. Biasanya berkaitan dengan ikatan tali atau bangunan. Dari sini lahir makna pelanggaran perjanjian yang telah disepakati seakan-akan ia mengurai tali yang sudah terikat atau menghancurkan bangunan yang sudah kokoh. Naq\u012b\u1e0d berarti suara yang terdengar saat memikul beban yang berat yang muncul dari alat pikul tersebut. Kokok ayam betina saat setelah bertelur diungkapkan dengan intaqa\u1e0dat ad-daj\u0101jah. Makna ini yang dimaksud dalam ayat ini yaitu beban berat yang dipikul punggung.  \nPada surat ini, Allah menjelaskan tentang nikmat atau anugerah yang diberikan Allah kepada Nabi Muhammad. Pada ayat sebelumnya, dijelaskan tentang nikmat as-syar\u1e25 atau pelapangan dada berupa rasa nyaman dan ketenangan yang dirasakan Muhammad dengan kehadiran Tuhannya. Pada ayat ini, Allah menambahkan nikmat-Nya dengan menghilangkan beban yang selama ini memberatkan punggung Muhammad. Kata anqa\u1e0da sampai punggungnya diungkapkan dengan suara kayu atau bambu. Ada beberapa pendapat mengenai beban berat yang diderita Muhammad, di antaranya adalah wafatnya istri dan paman beliau yaitu Khadijah dan Ab\u016b \u1e6c\u0101lib, beratnya wahyu yang diterimanya, dan beratnya keadaan masyarakat Jahiliah yang enggan menerima risalah yang dibawanya.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}