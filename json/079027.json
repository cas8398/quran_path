{
    "id": 5739,
    "surah_id": 79,
    "ayah": 27,
    "page": 584,
    "quarter_hizb": 59,
    "juz": 30,
    "manzil": 7,
    "arabic": "\u0621\u064e\u0627\u064e\u0646\u0652\u062a\u064f\u0645\u0652 \u0627\u064e\u0634\u064e\u062f\u0651\u064f \u062e\u064e\u0644\u0652\u0642\u064b\u0627 \u0627\u064e\u0645\u0650 \u0627\u0644\u0633\u0651\u064e\u0645\u064e\u0627\u06e4\u0621\u064f \u06da \u0628\u064e\u0646\u0670\u0649\u0647\u064e\u0627\u06d7",
    "latin": "A'antum asyaddu khalqan amis-sam\u0101'u ban\u0101h\u0101.",
    "translation": "Apakah penciptaan kamu yang lebih hebat ataukah langit yang telah dibangun-Nya?",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 79,
        "arabic": " \u0627\u0644\u0646\u0651\u0670\u0632\u0639\u0670\u062a",
        "latin": "An-N\u0101zi\u2018\u0101t",
        "transliteration": "An-Nazi\u2018at",
        "translation": "Yang Mencabut Dengan Keras",
        "num_ayah": 46,
        "page": 583,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Menjelaskan keperkasaan Allah dan kelemahan manusia, Allah berfrman, \u201cApakah penciptaan kamu yang lebih hebat ataukah langit yang telah dibangun-Nya? Secara logika, penciptaan langit yang demikian luas tentu lebih sulit daripada penciptaan manusia.",
        "tahlili": "Ayat ini menghimbau manusia untuk menggunakan akalnya untuk membandingkan penciptaan dirinya yang kecil dan lemah dengan penciptaan alam semesta yang demikian luas dan kokoh. Hal itu menunjukkan kekuasaan Allah. Ibnu Khaldun menggambarkan keadaan manusia yang terlalu mengagungkan kemampuan logika tanpa mengasah kalbunya dengan mengatakan, \u201cBagaimana manusia dengan otaknya yang hanya sebesar timbangan emas mau digunakan untuk menimbang alam semesta?\u201d ",
        "intro_surah": "Surah an-N\u0101zi\u2018\u0101t terdiri dari 46 ayat, termasuk kelompok surah Makkiyyah, diturunkan sesudah Surah an-Naba\u2019.\nNama an-N\u0101zi\u2018\u0101t (Malaikat-malaikat yang mencabut) diambil dari kata an-N\u0101zi\u2018\u0101t yang terdapat pada ayat pertama surah ini.\nSurah ini dinamai pula dengan as-S\u0101hirah yang diambil dari ayat 14, dan dinamai juga a\u1e6d-\u1e6c\u0101mmah yang diambil dari ayat 34.\n\nPokok-pokok Isinya:\n1.\tKeimanan:\nPenegasan Allah tentang adanya hari Kiamat dan sikap orang-orang musyrik terhadapnya; manusia dibagi dua golongan di akhirat; manusia tidak dapat mengetahui kapan terjadinya saat Kiamat.\n2.\tKisah:\nKisah Nabi Musa dengan Fir\u2018aun.",
        "outro_surah": null,
        "munasabah_prev_surah": "HUBUNGAN SURAH AN-NABA\u2019\nDENGAN SURAH AN-N\u0100ZI\u2018\u0100T\n\n1. \tSurah an-Naba\u2019 menerangkan ancaman Allah terhadap sikap orang-orang musyrik yang mengingkari adanya hari kebangkitan, serta mengemukakan bukti-bukti adanya hari kebangkitan, sedangkan pada Surah an-N\u0101zi\u2018\u0101t, Allah bersumpah bahwa hari Kiamat yang mendahului hari kebangkitan itu pasti terjadi.\n2. \tSama-sama menerangkan huru-hara yang terjadi pada hari Kiamat dan hari kebangkitan.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah mengisahkan kepada kaum musyrikin kisah Musa dengan Fir\u2018aun. Kisah itu memberikan isyarat dengan halus bahwa mereka tidak akan terlepas dari azab Allah jika Ia menghendaki; karena Fir\u2018aun yang kuat pun tidak luput dari azab-Nya. Kisah-kisah itu juga mengandung dorongan kepada Nabi Muhammad supaya tetap berlaku sabar dalam menghadapi tantangan kaum Quraisy. Pada ayat-ayat berikut ini, Allah memperingatkan orang-orang yang mengingkari hari kebangkitan bahwa mereka tidak pantas mengingkarinya, karena membangkitkan mereka dari kubur jika dibandingkan dengan penciptaan langit dan mengatur peredaran planet-planet, menurut pikiran manusia, jauh lebih besar dan lebih sulit daripada membangkitkan manusia dari kuburnya. Sedangkan bagi Allah semua itu mudah.",
        "theme_group": "MEMBANGKITKAN MANUSIA KEMBALI\nMUDAH BAGI ALLAH ",
        "kosakata": "Kosakata: \n1.\tAg\u1e6dasya \u0627\u064e\u063a\u0652\u0637\u064e\u0634\u064e (an-N\u0101zi\u2018\u0101t/79: 29)\nAg\u1e6dasya artinya membuat menjadi gelap. Allah berfirman dalam Surah an-N\u0101zi\u2018\u0101t/79: 29: Ag\u1e6dasya lailah\u0101 wa akhraja \u1e0du\u1e25\u0101h\u0101 (dan Dia menjadikan malamnya (gelap gulita), dan menjadikan siangnya (terang benderang). Dalam bahasa Arab terdapat kata al-ag\u1e6dasy yaitu orang yang lemah penglihatannya sehingga segalanya terlihat gelap.\n2.\tDa\u1e25\u0101h\u0101 \u062f\u064e\u062d\u0670\u0647\u064e\u0627 (an-N\u0101zi\u2018\u0101t/79: 30)\nDa\u1e25\u0101h\u0101 artinya menghamparkan. Dalam Surah an-N\u0101zi\u2018\u0101t/29: 30 dinyatakan bahwa bumi ini dihamparkan oleh Allah, artinya didatarkan-Nya dan diberi-Nya kelengkapan-kelengkapan kehidupan sehingga siap huni. Itu setelah sebelumnya bumi ini diciptakan-Nya secara \u201ckasar\u201d sehingga belum layak huni. Setelah itu, Allah naik untuk menciptakan langit, dan kemudian turun lagi untuk \u201cmenghaluskan\u201d bumi itu untuk layak huni.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}