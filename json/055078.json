{
    "id": 4979,
    "surah_id": 55,
    "ayah": 78,
    "page": 534,
    "quarter_hizb": 54,
    "juz": 27,
    "manzil": 7,
    "arabic": "\u062a\u064e\u0628\u0670\u0631\u064e\u0643\u064e \u0627\u0633\u0652\u0645\u064f \u0631\u064e\u0628\u0651\u0650\u0643\u064e \u0630\u0650\u0649 \u0627\u0644\u0652\u062c\u064e\u0644\u0670\u0644\u0650 \u0648\u064e\u0627\u0644\u0652\u0627\u0650\u0643\u0652\u0631\u064e\u0627\u0645\u0650 \u08d6 ",
    "latin": "Tab\u0101rakasmu rabbika \u017cil-jal\u0101li wal-ikr\u0101m(i).",
    "translation": " Maha Berkah nama Tuhanmu Pemilik keagungan dan kemuliaan.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 55,
        "arabic": "\u0627\u0644\u0631\u0651\u062d\u0645\u0670\u0646",
        "latin": "Ar-Ra\u1e25m\u0101n",
        "transliteration": "Ar-Rahman",
        "translation": "Yang Maha Pengasih",
        "num_ayah": 78,
        "page": 531,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Demikianlah nikmat-nikmat Tuhanmu, wahai manusia dan jin. Akhirnya, wahai Nabi Muhammad, Mahasuci nama Tuhanmu, pemilik keagungan dan kemuliaan.",
        "tahlili": "Ayat ini mengungkapkan bahwa, hanya Allah-lah yang mempunyai kebesaran dan karunia atas segala nikmat yang diberikan-Nya, nikmat yang sangat bayak dan ganjaran yang sangat berharga.\nIni adalah pelajaran bagi hamba-Nya bahwa semuanya itu adalah rahmat-Nya. Dia yang menjadikan langit dan bumi, surga dan neraka, menyiksa orang-orang berdosa, memberi pahala kepada orang-orang yang menaati-Nya.",
        "intro_surah": "Surah ar-Ra\u1e25m\u0101n terdiri dari 78 ayat, termasuk kelompok surah Madaniyyah, diturunkan sesudah Surah ar-Ra\u2018d.\nDinamai ar-Ra\u1e25m\u0101n (Yang Maha Pemurah), diambil dari kata ar-ra\u1e25m\u0101n yang terdapat pada ayat pertama surah ini. Ar-Ra\u1e25m\u0101n adalah salah satu dari nama-nama Allah. Sebagian besar dari isi surah ini menerangkan kemurahan Allah kepada hamba-hamba-Nya, dengan memberikan nikmat-nikmat yang tidak terhingga kepada mereka baik di dunia maupun di akhirat nanti.\n\nPokok-pokok Isinya:\n1.\tKeimanan:\nAllah menciptakan manusia dan mengajar mereka berbicara; alam semesta tunduk kepada Allah; semua makhluk akan hancur kecuali Allah; Allah selalu sibuk bekerja mentadbirkan alam; seluruh alam merupakan nikmat Allah terhadap umat manusia; manusia diciptakan dari tanah dan jin dari api.\n2.\tHukum-hukum:\n\tKewajiban memenuhi ukuran, takaran, dan timbangan.\n3.\tLain-lain:\nManusia dan jin tidak akan mampu lari dari kekuasaan Allah, banyak umat manusia yang tidak mensyukuri nikmat Allah; memberitakan tentang keajaiban-keajaiban alam sebagai bukti kekuasaan Allah.",
        "outro_surah": "Surah ar-Ra\u1e25m\u0101n menyebutkan bermacam-macam nikmat Allah yang telah dilimpahkan kepada hamba-hamba-Nya yaitu dengan menciptakan alam dengan segala yang ada padanya. Kemudian diterangkan pembalasan di akhirat, keadaan penghuni neraka dan keadaan penghuni surga, dan diterang-kan pula keadaan di dalam surga yang dijanjikan Allah bagi orang yang bertakwa.",
        "munasabah_prev_surah": "1. \tSurah al-Qamar menerangkan tentang adanya hari Kiamat dan apa yang akan dirasakan manusia yang baik di surga, dan manusia yang jahat di neraka. Surah ar-Ra\u1e25m\u0101n menerangkan secara lebih luas kenikmatan hidup di surga.\n2. \tSurah al-Qamar menerangkan azab yang ditimpakan kepada umat-umat terdahulu yang mendurhakai nabi-nabi mereka, Surah ar-Ra\u1e25m\u0101n menye-butkan berbagai nikmat Allah yang dilimpahkan-Nya kepada jin dan manusia sebagai hamba-hamba-Nya, agar mereka memilih beriman.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah swt menerangkan ganjaran yang diberikan kepada orang-orang yang bertakwa kepada-Nya berupa surga baik rohani maupun jasmani, maka dalam ayat-ayat berikut ini, Allah swt mengungkapkan tambahan ganjaran bagi penghuni surga untuk orang-orang yang bertakwa. Dua surga lagi selain yang telah dianugerahkan sebelumnya.",
        "theme_group": "TAMBAHAN GANJARAN BAGI ORANG MUKMIN\nPADA HARI KIAMAT ",
        "kosakata": "Kosakata:\n1.\tMudh\u0101mmat\u0101ni \u0645\u064f\u062f\u0652\u0647\u064e\u0627\u0645\u0651\u064e\u062a\u064e\u0627\u0646\u0650 (ar-Ra\u1e25m\u0101n/55: 64)\nKata mudh\u0101mmat\u0101ni merupakan bentuk dua (mu\u1e61anna) dari kata mudh\u0101mmah. Kata ini sendiri terambil dari ad-duhmah, yang pada awalnya diartikan sebagai gelapnya malam. Selain itu, kata ini juga diberi makna sebagai hijau tua yang pekat sehingga sangat dekat dengan warna hitam. Kata mudh\u0101mmat\u0101ni dipergunakan untuk memberikan sifat kepada banyak-nya pepo-honan yang sangat rimbun dengan dedaunan di surga. Karena itu suasana surga tampak menghijau pekat.\n2. Na\u1e0d\u1e0d\u0101khat\u0101ni \u0646\u064e\u0636\u0651\u064e\u0627\u062e\u064e\u062a\u064e\u0627\u0646\u0650 (ar-Ra\u1e25m\u0101n/55: 66) \nKata na\u1e0d\u1e0d\u0101khat\u0101n merupakan bentuk dua (mu\u1e61anna) dari na\u1e0d\u1e0d\u0101khah, yang artinya yang memancar. Kata ini dipergunakan untuk menggambarkan adanya dua mata air di surga yang selalu memancarkan air. Surga yang dipenuhi dengan pepohonan yang rimbun dengan dedaunan, ditambah adanya mata air yang selalu memancarkan air, merupakan pemandangan yang sangat indah dan tiada taranya. Keadaan seperti ini yang akan ditemukan oleh mereka yang mendapat balasan surga.\n3.\u2018Abqariyy \u0639\u064e\u0628\u0652\u0642\u064e\u0631\u0650\u064a\u0651 (ar-Ra\u1e25m\u0101n/55: 76)\nKata \u2018abqariyy berasal dari kata \u2018abqar. Yang artinya sesuatu yang luar biasa. Pada masa dahulu, kata \u2018abqar diartikan sebagai tempat permukiman para jin. Bangsa Arab pada masa itu juga mempercayai bahwa semua yang indah-indah atau yang tidak mampu dilakukan manusia adalah hasil karya jin. Dari kebiasaan ini, selanjutnya muncul pengertian bahwa segala sesuatu yang mencapai puncak keindahan dan keistimewaan disebut \u2018abqariy. Orang jenius disebut \u2018abqariy, karena ia memiliki kemampuan akal atau kecerdasan yang luar biasa. Kata ini disebut dalam ayat untuk menggambarkan keindahan surga dan isinya yang luar biasa.",
        "sabab_nuzul": null,
        "conclusion": "1.\tAllah memberikan ganjaran berupa dua surga kepada orang-orang yang bertakwa kepada-Nya dari golongan al-Muqarrab\u012bn dan surga yang diberikan kepada golongan A\u1e63\u1e25\u0101bul-yam\u012bn.\n2.\tSurga dipenuhi dengan segala macam nikmat yang tidak pernah dilihat oleh mata, tidak pernah didengar oleh telinga dan tidak pernah terbayang dalam pikiran manusia.\n3.\tAllah swt Mahaagung dan Mahasuci. Dialah pemberi nikmat dan rahmat kepada hamba-hamba-Nya."
    }
}