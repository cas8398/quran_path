{
    "id": 1415,
    "surah_id": 10,
    "ayah": 51,
    "page": 214,
    "quarter_hizb": 22,
    "juz": 11,
    "manzil": 3,
    "arabic": "\u0627\u064e\u062b\u064f\u0645\u0651\u064e \u0627\u0650\u0630\u064e\u0627 \u0645\u064e\u0627 \u0648\u064e\u0642\u064e\u0639\u064e \u0627\u0670\u0645\u064e\u0646\u0652\u062a\u064f\u0645\u0652 \u0628\u0650\u0647\u0656\u06d7  \u0627\u0670\u06e4\u0644\u0652\u0640\u0654\u0670\u0646\u064e \u0648\u064e\u0642\u064e\u062f\u0652 \u0643\u064f\u0646\u0652\u062a\u064f\u0645\u0652 \u0628\u0650\u0647\u0656 \u062a\u064e\u0633\u0652\u062a\u064e\u0639\u0652\u062c\u0650\u0644\u064f\u0648\u0652\u0646\u064e ",
    "latin": "A\u1e61umma i\u017c\u0101 m\u0101 waqa\u2018a \u0101mantum bih(\u012b), \u0101l'\u0101na wa qad kuntum bih\u012b tasta\u2018jil\u016bn(a). ",
    "translation": "Apabila azab itu terjadi, apakah kemudian kamu baru memercayainya? Apakah (baru) sekarang (kamu beriman), padahal sebelumnya kamu selalu meminta agar ia disegerakan?",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 10,
        "arabic": " \u064a\u0648\u0646\u0633",
        "latin": "Y\u016bnus",
        "transliteration": "Yunus",
        "translation": "Yunus",
        "num_ayah": 109,
        "page": 208,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Kemudian apakah setelah azab itu terjadi, kamu baru mempercayainya bahwa Allah Mahakuasa? Padahal saat itu kepercayaanmu sudah tidak berguna, karena kamu sudah binasa. Apakah baru sekarang kamu percaya kalau azab itu pasti datang, padahal sebelumnya kamu selalu meminta agar azab itu disegerakan? Ayat ini menjadi teguran bagi orang yang sombong bahkan menantang agar segera didatangkan azab yang diancamkan kepada orang-orang yang durhaka.",
        "tahlili": "Di ayat ini, Allah memerintahkan kepada Rasul agar menanyakan kepada mereka apakah orang-orang musyrik itu baru mau mempercayai ancaman Allah untuk mengazab mereka setelah terjadi azab yang mereka takuti. Padahal pada saat itu keimanan mereka tidak berguna lagi, lalu apa gunanya mereka selalu meminta supaya siksa yang dijanjikan kepada mereka itu disegerakan, ataukah permintaan menyegerakan itu hanyalah untuk menunjukkan sikap mereka yang selalu mendustakan ayat-ayat Allah dan menanggapi dengan kesombongan?",
        "intro_surah": "Surah Y\u016bnus terdiri dari 109 ayat, termasuk golongan surah-surah Makiyah, diturunkan sesudah Surah al-Isr\u0101\u2019 dan sebelum Surah H\u016bd, seperti diutarakan as-Suy\u016b\u1e6d\u012b dalam kitabnya \u201cAl-Itq\u0101n\u201d, kecuali ayat 40, 94, dan 95. Ketiganya diturunkan di Medinah, setelah Nabi Muhammad berhijrah.\n\tSurah ini dinamai \u201cSurah Y\u016bnus\u201d, karena dalam surah ini dikemukakan kisah Nabi Yunus a.s. dengan pengikutnya yang teguh imannya.\n\nPokok-pokok Isinya:\n1.\tKeimanan\nKeesaan Allah baik zat-Nya maupun kekuasaan dan penciptaan; Al-Qur\u2019an bukanlah sihir; Allah mengatur semesta alam dari Arasy-Nya syafa\u2019at hanyalah dengan izin Allah; wahyu Allah menerangkan semua yang gaib bagi manusia; wali-wali Allah. Allah mengawasi dan mengamati perbuatan hamba-hamba-Nya di dunia, Allah tidak mempunyai anak.\n\n2.\tHukum-hukum\nMenentukan perhitungan tahun dan waktu dengan perjalanan matahari dan bulan, hukum mengadakan sesuatu terhadap Allah dan hukum mendustakan ayat-ayat-Nya.\n\n3.\tKisah-kisah\nKisah Nabi Nuh as dengan kaumnya; kisah Nabi Musa a.s. dengan Fir\u2018aun dan tukang sihir; kisah Bani Israil setelah keluar dari negeri Mesir; kisah Nabi Yunus as dengan kaumnya.\n\n4.\tLain-lain\nHari kebangkitan dan hari pembalasan; tentang sifat-sifat manusia, seperti suka tergesa-gesa, ingat kepada Allah diwaktu sukar dan lupa kepadanya di waktu senang, ingkar kepada nikmat Allah, suka kepada purbasangka dan sebagainya; keadaan orang-orang berbuat baik dan orang-orang berbuat jahat di Hari Kiamat; tidak seorang pun yang dapat membuat seperti Al-Qur\u2019an, tugas Rasul hanyalah menyampaikan risalah yang dibawanya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tAkhir Surah at-Taubah ditutup dengan menyebutkan risalah Nabi Muhammad saw dan hal-hal yang serupa disebutkan pula oleh Surah Y\u016bnus.\n2.\tSurah at-Taubah menerangkan keadaan orang-orang munafik, dan perbuatan mereka di waktu Al-Qur\u2019an diturunkan, sedang Surah Y\u016bnus menerangkan sikap orang-orang kafir terhadap Al-Qur\u2019an.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu, Allah menerangkan sifat-sifat orang musyrik. Mereka mendustakan Al-Qur\u2019an serta tidak mau memikirkan kebenaran isinya. Mereka bersikap tergesa-gesa memberikan penilaian terhadap firman Allah, sebelum memperhatikan sebaik-baiknya bukti kebenarannya. Pada ayat-ayat ini, Allah mengancam orang-orang yang akan merasakan azab yang pedih nanti pada Hari Kiamat.",
        "theme_group": "ANCAMAN ALLAH TERHADAP ORANG-ORANG YANG\nMENDUSTAKAN AYAT-AYAT AL-QUR\u2019AN",
        "kosakata": "Kosakata: Liq\u0101\u2019 Allah \u0644\u0650\u0642\u064e\u0627\u0621 \u0627\u0644\u0644\u0651\u0670\u0647 (Y\u016bnus/10: 45)\nAl-Liq\u0101\u2019 masdar dari laqiya-yalqa artinya bertemu sesuatu baik sesuatu bersifat inderawi atau bersifat abstrak, seperti firman Allah: walaqad kuntum tamannauna al-mauta min qabli an talqauhu (\u0648\u064e\u0644\u064e\u0642\u064e\u062f\u0652 \u0643\u064f\u0646\u0652\u062a\u064f\u0645\u0652 \u062a\u064e\u0645\u064e\u0646\u0651\u064e\u0648\u0652\u0646\u064e \u0627\u0644\u0652\u0645\u064e\u0648\u0652\u062a\u064e \u0645\u0650\u0646\u0652 \u0642\u064e\u0628\u0652\u0644\u0650 \u0627\u064e\u0646\u0652 \u062a\u064e\u0644\u0652\u0642\u064e\u0648\u0652\u0647\u064f) (\u0100li \u2018Imr\u0101n/3: 143)\nKata liq\u0101\u2019 Allah dalam Al-Qur\u2019an merujuk pada arti kiamat, kebangkitan, hisab dan nasib manusia di akhirat. Liq\u0101\u2019 Allah sebagai Hari Kiamat karena pada hari itulah manusia bertemu dengan Allah dan malaikat serta manusia-manusia lainnya, juga dengan amal baik buruknya yang pernah diperbuat di dunia. ",
        "sabab_nuzul": null,
        "conclusion": null
    }
}