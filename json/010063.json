{
    "id": 1427,
    "surah_id": 10,
    "ayah": 63,
    "page": 216,
    "quarter_hizb": 22.25,
    "juz": 11,
    "manzil": 3,
    "arabic": "\u0627\u064e\u0644\u0651\u064e\u0630\u0650\u064a\u0652\u0646\u064e \u0627\u0670\u0645\u064e\u0646\u064f\u0648\u0652\u0627 \u0648\u064e\u0643\u064e\u0627\u0646\u064f\u0648\u0652\u0627 \u064a\u064e\u062a\u0651\u064e\u0642\u064f\u0648\u0652\u0646\u064e\u06d7  ",
    "latin": "Al-la\u017c\u012bna \u0101man\u016b wa k\u0101n\u016b yattaq\u016bn(a). ",
    "translation": "(Mereka adalah) orang-orang yang beriman dan selalu bertakwa.",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 10,
        "arabic": " \u064a\u0648\u0646\u0633",
        "latin": "Y\u016bnus",
        "transliteration": "Yunus",
        "translation": "Yunus",
        "num_ayah": 109,
        "page": 208,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Wali-wali Allah yang disebut pada ayat sebelumnya, yaitu orang-orang yang beriman tanpa ada sedikit pun keraguan dan senantiasa bertakwa, yakni selalu mengerjakan amal saleh sehingga mereka terhindar dari azab Allah.",
        "tahlili": "Allah menjelaskan siapa yang dimaksud dengan wali-wali Allah yang berbahagia itu dan apa sebabnya mereka demikian. Penjelasan yang didapat dalam ayat ini menunjukkan bahwa wali itu ialah orang-orang yang beriman dan bertakwa. Dimaksud beriman di sini ialah orang yang beriman kepada Allah, kepada malaikat-Nya, kepada kitab-kitab-Nya, kepada rasul-rasul-Nya, kepada hari akhir, segala kejadian yang baik dan yang buruk semuanya dari Allah, serta melaksanakan apa yang seharusnya dilakukan oleh orang-orang yang beriman. Sedang yang dimaksud dengan bertakwa ialah memelihara diri dari segala tindakan yang bertentangan dengan hukum-hukum Allah, baik hukum-hukum Allah yang mengatur tata alam semesta, ataupun hukum syara\u2019 yang mengatur tata hidup manusia di dunia (lihat tafsir Surah al-Anf\u0101l/8: 10). ",
        "intro_surah": "Surah Y\u016bnus terdiri dari 109 ayat, termasuk golongan surah-surah Makiyah, diturunkan sesudah Surah al-Isr\u0101\u2019 dan sebelum Surah H\u016bd, seperti diutarakan as-Suy\u016b\u1e6d\u012b dalam kitabnya \u201cAl-Itq\u0101n\u201d, kecuali ayat 40, 94, dan 95. Ketiganya diturunkan di Medinah, setelah Nabi Muhammad berhijrah.\n\tSurah ini dinamai \u201cSurah Y\u016bnus\u201d, karena dalam surah ini dikemukakan kisah Nabi Yunus a.s. dengan pengikutnya yang teguh imannya.\n\nPokok-pokok Isinya:\n1.\tKeimanan\nKeesaan Allah baik zat-Nya maupun kekuasaan dan penciptaan; Al-Qur\u2019an bukanlah sihir; Allah mengatur semesta alam dari Arasy-Nya syafa\u2019at hanyalah dengan izin Allah; wahyu Allah menerangkan semua yang gaib bagi manusia; wali-wali Allah. Allah mengawasi dan mengamati perbuatan hamba-hamba-Nya di dunia, Allah tidak mempunyai anak.\n\n2.\tHukum-hukum\nMenentukan perhitungan tahun dan waktu dengan perjalanan matahari dan bulan, hukum mengadakan sesuatu terhadap Allah dan hukum mendustakan ayat-ayat-Nya.\n\n3.\tKisah-kisah\nKisah Nabi Nuh as dengan kaumnya; kisah Nabi Musa a.s. dengan Fir\u2018aun dan tukang sihir; kisah Bani Israil setelah keluar dari negeri Mesir; kisah Nabi Yunus as dengan kaumnya.\n\n4.\tLain-lain\nHari kebangkitan dan hari pembalasan; tentang sifat-sifat manusia, seperti suka tergesa-gesa, ingat kepada Allah diwaktu sukar dan lupa kepadanya di waktu senang, ingkar kepada nikmat Allah, suka kepada purbasangka dan sebagainya; keadaan orang-orang berbuat baik dan orang-orang berbuat jahat di Hari Kiamat; tidak seorang pun yang dapat membuat seperti Al-Qur\u2019an, tugas Rasul hanyalah menyampaikan risalah yang dibawanya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1.\tAkhir Surah at-Taubah ditutup dengan menyebutkan risalah Nabi Muhammad saw dan hal-hal yang serupa disebutkan pula oleh Surah Y\u016bnus.\n2.\tSurah at-Taubah menerangkan keadaan orang-orang munafik, dan perbuatan mereka di waktu Al-Qur\u2019an diturunkan, sedang Surah Y\u016bnus menerangkan sikap orang-orang kafir terhadap Al-Qur\u2019an.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu Allah menjelaskan kekuasaan ilmu-Nya, dan ketelitian-Nya dalam menilai amal perbuatan hamba-Nya. Serta disebutkan-Nya pula beberapa kenikmatan yang telah diberikan-Nya kepada manusia, dan kewajiban apa yang harus dilakukan oleh para hamba-Nya pada saat menerima kenikmatan yang beraneka ragam itu. Maka pada ayat ini Allah menyebutkan sikap orang-orang takwa yang mensyukuri nikmat-nikmat Allah dan berita gembira yang akan mereka terima. Mereka itu akan mendapatkan pahala, yang lebih baik dari kenikmatan yang pernah mereka terima di dunia.",
        "theme_group": "BERITA GEMBIRA BAGI WALIYULLAH",
        "kosakata": "Kosakata: al-\u2019Izzah \u0627\u0644\u0639\u0632\u0651\u0629 (Y\u016bnus/10: 65)\nAl-\u2019Izzah masdar dari \u2018azza-ya\u2019izzu merujuk pada dua arti: pertama, kata kekuatan, kekuasaan dan kehebatan pada seseorang sehingga tidak bisa dikalahkan. Kedua, sedikit, sesuatu yang karena sedikitnya menjadi mulia. Oleh sebab itu, kata al-\u2019az\u012bz bisa berarti perkasa atau mulia. Al-\u2019Izzah digunakan dalam Al-Qur\u2019an dalam memuji Allah, rasul, dan orang-orang yang beriman. Sebagaimana firman-Nya pada Surah al-Mun\u0101fiq\u016bn/63: 8, \u0648\u064e\u0644\u0650\u0644\u0651\u064e\u0647\u0650 \u0627\u0644\u0652\u0639\u0650\u0632\u0651\u064e\u0629\u064f \u0648\u064e\u0644\u0650\u0631\u064e\u0633\u064f\u0648\u0644\u0650\u0647\u0650 \u0648\u064e\u0644\u0650\u0644\u0652\u0645\u064f\u0624\u0652\u0645\u0650\u0646\u0650\u064a\u0646\u064e. Dalam Al-Qur\u2019an ada kata al-\u2019izzah yang dipinjam untuk menunjukkan kesombongan orang-orang kafir seperti pada firman Allah pada Surah al-Baqarah/2: 206 \u0623\u064e\u062e\u064e\u0630\u064e\u062a\u0652\u0647\u064f \u0627\u0644\u0652\u0639\u0650\u0632\u0651\u064e\u0629\u064f \u0628\u0650\u0627\u0644\u0652\u0625\u0650\u062b\u0652\u0645\u0650 Kata al-\u2019izzah terulang sebanyak 10 kali diantaranya pada Surah an-Nis\u0101\u2019/4: 139 ",
        "sabab_nuzul": null,
        "conclusion": null
    }
}