{
    "id": 5358,
    "surah_id": 69,
    "ayah": 35,
    "page": 568,
    "quarter_hizb": 57.5,
    "juz": 29,
    "manzil": 7,
    "arabic": "\u0641\u064e\u0644\u064e\u064a\u0652\u0633\u064e \u0644\u064e\u0647\u064f \u0627\u0644\u0652\u064a\u064e\u0648\u0652\u0645\u064e \u0647\u0670\u0647\u064f\u0646\u064e\u0627 \u062d\u064e\u0645\u0650\u064a\u0652\u0645\u064c\u06d9",
    "latin": "Fa laisa lahul-yauma h\u0101hun\u0101 \u1e25am\u012bm(un).",
    "translation": "Maka, pada hari ini tidak ada seorang pun teman setia baginya di sini (neraka).",
    "no_footnote": null,
    "footnotes": null,
    "surah": {
        "id": 69,
        "arabic": " \u0627\u0644\u062d\u0627\u06e4\u0642\u0651\u0629",
        "latin": "Al-\u1e24\u0101qqah",
        "transliteration": "Al-Haqqah",
        "translation": "Hari Kiamat Yang Pasti Terjadi",
        "num_ayah": 52,
        "page": 566,
        "location": "Makkiyah"
    },
    "tafsir": {
        "wajiz": "Atas segala perbuatan buruk yang dilakukan di dunia, maka akhirnya pada hari ini yaitu hari di akhirat, di sini yaitu di neraka, tidak ada seorang teman pun baginya yang dapat menolong atau meringankan siksa yang dia terima.",
        "tahlili": "Dalam ayat ini diterangkan keadaan orang musyrik di dalam neraka:\n1. \tMereka tidak mempunyai seorang pun teman atau penolong. Sebagaimana diketahui bahwa manusia itu adalah makhluk sosial. Hidup manusia yang berbahagia adalah jika mereka dapat memenuhi kepentingan pribadinya dan kepentingan hidup dalam pergaulan bermasyarakat. Jika di dunia dalam keadaan biasa, manusia merasa tersiksa hidup sendirian, tentu di akhirat akan lebih tersiksa lagi.\n2. \tMakanan mereka adalah darah dan nanah, suatu makanan yang tidak termakan oleh orang ketika hidup di dunia.",
        "intro_surah": "Surah ini terdiri dari 52 ayat, termasuk surah Makkiyyah dan diturunkan sesudah surah al-Mulk. Surah ini bernama al-\u1e24\u0101qqah yang artinya hari Kiamat, diambil dari kata al-\u1e24\u0101qqah pada ayat pertama, kedua dan ketiga.\nDalam Al-Qur\u2019an ada beberapa surah yang namanya berarti hari Kiamat, seperti al-W\u0101qi\u2018ah/56, al-\u1e24\u0101qqah/69, dan al-Qiy\u0101mah/75. Meskipun kata-kata yang digunakan mempunyai arti bahasa yang berbeda-beda, tetapi maksudnya satu yaitu hari Kiamat. Hal ini menunjukkan pentingnya memperhatikan dan mempersiapkan diri dengan beriman yang mantap dan beramal saleh untuk menghadapi hari Kiamat.\n\nPokok-pokok Isinya:\nPeringatan terhadap azab yang ditimpakan kepada kaum Nuh, Samud, \u2018Ad, Fir\u2018aun, dan kaum-kaum sebelum mereka yang durhaka kepada Allah dan rasul-Nya pada hari Kiamat; kejadian-kejadian pada hari Kiamat dan hari penghisaban; penegasan Allah bahwa Al-Qur\u2019an itu benar-benar wahyu-Nya.",
        "outro_surah": null,
        "munasabah_prev_surah": "1. \tDalam Surah al-Qalam disebutkan tentang hari Kiamat secara umum, sedang dalam Surah al-\u1e24\u0101qqah dijelaskan secara terperinci peristiwa-peristiwa hari Kiamat itu.\n2. \tDalam Surah al-Qalam diterangkan orang-orang yang mendustakan Al-Qur\u2019an dan ancaman azab atas mereka, sedangkan dalam Surah al-\u1e24\u0101qqah diterangkan bahwa orang-orang zaman dahulu yang mendustakan Rasul-rasul dan macam-macam azab yang telah menimpa mereka.\n3. \tDalam Surah al-Qalam, Allah membantah tuduhan orang-orang musyrik Mekah bahwa Muhammad saw orang gila, sedang dalam Surah al-\u1e24\u0101qqah, Allah membantah tuduhan bahwa Nabi Muhammad seorang penyair.",
        "munasabah_prev_theme": "Pada ayat-ayat yang lalu diterangkan keadaan orang mukmin waktu diadakan penghisaban di hari akhirat. Mereka bergembira membaca catatan amalnya dan diterangkan pula keadaan kehidupan dan tempat tinggal mereka di dalam surga. Pada ayat-ayat berikut ini disebutkan keadaan orang kafir waktu dihisab. Mereka bersedih ketika membaca catatan amalnya karena membayangkan beratnya azab yang akan menimpa mereka. Kemudian diterangkan bahwa mereka seperti itu karena tidak beriman dan beramal sewaktu hidup di dunia.",
        "theme_group": "KEADAAN ORANG KAFIR PADA HARI PERHITUNGAN",
        "kosakata": "Kosakata: \n1.\t\u1e62all\u016bhu \u0635\u064e\u0644\u0651\u064f\u0648\u0652\u0647\u064f (al-\u1e24\u0101qqah/69: 31)\n\t\u1e62all\u016bhu adalah bentuk fi\u2018il amr (kata perintah) yang artinya: masukkan, pangganglah! Berasal dari fi\u2019il \u1e63al\u0101-ya\u1e63l\u0101-\u1e63alyan artinya: memanggang, memasukkan ke dalam api. Ayat 31 yang sangat pendek ini: \u1e61ummal-ja\u1e25\u012bma \u1e63all\u016bhu (kemudian masukkan dia ke dalam api neraka yang menyala-nyala) merupakan rangkaian kisah orang kafir di akhirat nanti. Setelah diberi kitab catatan amal perbuatannya dari sebelah kiri, orang kafir merasa sangat kecewa sampai berpikir lebih baik tidak menerimanya, karena isinya memang jelek sekali seperti kejelekan dan keburukan perbuatan mereka di dunia. Tetapi demikianlah kekufuran dan kejahatan perbuatan mereka di dunia dan kini catatan perbuatan buruk itu ternyata juga begitu, dia menyesal sekali, tetapi sudah tidak ada gunanya. Maka Allah memerintahkan kepada malaikat untuk membelenggu leher orang kafir ini dengan rantai besi yang panjang, kemudian memasukkannya ke dalam api yang menyala-nyala yaitu neraka Jahim.\n2.\tGisl\u012bn \u063a\u0650\u0633\u0652\u0644\u0650\u064a\u0652\u0646\u064c (al-\u1e24\u0101qqah/69: 36)\n\tGisl\u012bn artinya adalah cairan yang mengalir dari daging atau tubuh manusia berupa darah dan nanah. Fi\u2018il (kata kerja) gasala-yagsilu-gaslan artinya mencuci atau membasuh. Adapun igtasala-yagtasilu-igtis\u0101lan artinya mandi. Al-Gas\u012bl atau al-gas\u012blah artinya yang dicuci atau yang dibasuh. Pada ayat 36 Surah al-\u1e24\u0101qqah/69 ini, Allah menjelaskan keadaan orang kafir setelah dihisab. Karena ketika di dunia, selain tidak beriman kepada Allah, mereka juga tidak melakukan kebaikan yang diperintahkan Allah, termasuk memberi makan orang fakir dan miskin, maka di akhirat, mereka tidak mempunyai seorang teman pun, dan juga tidak mempunyai makanan sedikit pun, kecuali hanya darah dan nanah saja.",
        "sabab_nuzul": null,
        "conclusion": null
    }
}